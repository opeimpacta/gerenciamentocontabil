<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- style BootStrap -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>" />
<link href="<c:url value="/resources/css/bootstrap_.css"/>"	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/calendar.css"/>" />
<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/custom_2.css"/>" />

<!-- style AUTOCOMPLETE -->
<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/autocomplete/styles.css"/>" />

<script src="<c:url value="/resources/js/jquery/jquery-2.1.3.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/modernizr.custom.63321.js"/>"></script>

<!-- JS e CSS do Chart -->
<script src="<c:url value="/resources/js/chart/Chart.js"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chart/chart.css"/>" />

<script	src="<c:url value="/resources/js/autocomplete/jquery.autocomplete.min.js"/>"></script>
