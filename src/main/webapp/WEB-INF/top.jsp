<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">

		<img class="logo" src="<c:url value="/resources/img/logo.png"/>"
			alt="Sistema Gerenciamento Contabil">

		<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target="#navbar" aria-expanded="false"
			aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<div id="navbar" class="navbar-collapse collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<li><a href="<c:url value="/home"/>">Home</a></li>
					<sec:authorize access="isAuthenticated()">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								Usu�rios
								<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
								<li>
									<a href="<c:url value="/user/create"/>">Cadastro de Usu�rio</a>
								</li>
								
								<li>
									<a href="<c:url value="/user/list"/>">Gerenciamento de Usu�rios</a>
								</li>
						</li>
					</sec:authorize>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Movimentos
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="<c:url value="/mescritorio"/>">Movimento
								Escrit�rio</a></li>
						<li><a href="<c:url value="/mcliente"/>">Movimento
								Clientes</a></li>
						<li><a href="<c:url value="/msocio"/>">Movimento
								S�cios</a></li>
						<li><a href="<c:url value="/mbanco"/>">Movimento
								Bancos</a></li>
					</ul>
				</li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Relat�rios<span
						class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="<c:url value="/relatorio/geral"/>">Fechamento
								Geral</a></li>
						<li><a href="<c:url value="/relatorio/caixa"/>">Fechamento
								Caixa</a></li>
					</ul>
					
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Clientes
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
					
					<li>
							<a href="<c:url value="/cliente/create"/>">Cadastro de Clientes</a>
						</li>
					
						<li>
							<a href="<c:url value="/cliente/list"/>">Gerenciamento de Clientes</a>
						</li>
					</ul>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Bancos
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
					
					<li>
							<a href="<c:url value="/banco/create"/>">Cadastro de Bancos</a>
						</li>
					
						<li>
							<a href="<c:url value="/banco/list"/>">Gerenciamento de Bancos</a>
						</li>
					</ul>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						S�cios
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
					
					<li>
							<a href="<c:url value="/socio/create"/>">Cadastro de S�cios</a>
						</li>
					
						<li>
							<a href="<c:url value="/socio/list"/>">Gerenciamento de S�cios</a>
						</li>
					
						
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> 
						<span class="glyphicon glyphicon-user"></span>
							<sec:authorize access="isAuthenticated()">
								<sec:authentication property="principal" var="user" /> 
								${user.name} 
							</sec:authorize>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="configuracao.html"><span class="glyphicon glyphicon-cog"></span> Configura��o</a>
						</li>
						<li>
							<a href="<c:url value="/logout"/>">
							<span class="glyphicon glyphicon-off"></span> Logout</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
