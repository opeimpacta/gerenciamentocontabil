<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<fmt:message key="message.password" var="noPass" />
<fmt:message key="message.username" var="noUser" />
<c:if test="${param.error != null}">
    <c:choose>
        <c:when
            test="${SPRING_SECURITY_LAST_EXCEPTION.message == 'User is disabled'}">
            <div class="alert alert-danger">
                <spring:message code="auth.message.disabled"></spring:message>
            </div>
        </c:when>
        <c:when
            test="${SPRING_SECURITY_LAST_EXCEPTION.message == 'User account has expired'}">
            <div class="alert alert-danger">
                <spring:message code="auth.message.expired"></spring:message>
            </div>
        </c:when>
        <c:when
            test="${SPRING_SECURITY_LAST_EXCEPTION.message == 'blocked'}">
            <div class="alert alert-danger">
               <spring:message code="auth.message.blocked"></spring:message>
            </div>
        </c:when>
        <c:otherwise>
            <div class="alert alert-danger">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <!-- <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/> -->
                <spring:message code="message.badCredentials"></spring:message>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>

<!DOCTYPE html>
<html>
	<head>
		<c:import url="/WEB-INF/header.jsp"/>
	</head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Sistema de Gerenciamento Contábil</title>
	
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
		    <div class="container">
		    	<img class="logo" src="<c:url value="/resources/img/logo.png"/>" alt="Sistema Gerenciamento Contabil">
		    	
		    </div>
	    </nav>
	    <c:if test="${param.message != null}">
			<div class="alert alert-info">
				${param.message}
			</div>
		</c:if>
	    <div class="row-fluid">
		    <div class="col-md-2"></div>
		    <div class="col-md-8">
			  		<div class="box_login">
				    <form:form servletRelativeAction="login" cssClass="form-signin">
				    	<h2 class="form-signin-heading">Acesso Restrito</h2>
				    	<p>
				    		<label for="username" class="sr-only">Email address</label>
				    		Usuário:<input type="email" class="form-control1" placeholder="Usuário" name="username"/>
				    	</p>
				    	<p>
				    		<label for="password" class="sr-only">Password</label>
				    		Senha:<input type="password" id="password" class="form-control1" placeholder="Senha" required name="password"/>
				    	</p>
				    	<div class="checkbox"></div>
				    	<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
				    </form:form>
			    </div>
		    </div>
		    <div class="col-md-2"></div>
	    </div>
	</body>
	<script type="text/javascript">
		function validate() {
			var password = $('#idpassword').val();
			var username = $('#idusername').val();
			
		    if (username == "" && password == "") {
		        alert("${noUser} & ${noPass}.");
		        $('#idusername').focus();
		        return false;
		    }
		    if (username == "") {
		        alert("${noUser}");
		        $('#idusername').focus();
		        return false;
		    }
		    if (password == "") {
			    alert("${noPass}");
			    $('#idpassword').focus();
		        return false;
		    }
		}
	</script>
</html>