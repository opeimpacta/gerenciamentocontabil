<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html>

<html>
	<head>
	<meta charset="UTF-8" >
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/calendar/fullcalendar.min.css"/>" />
        
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/calendar/bootstrap-colorpicker.min.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/calendar/bootstrap-timepicker.min.css"/>" />
        
        
        <script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.min.js'></script>
        
        <script src="<c:url value="/resources/js/calendar/bootstrap-timepicker.min.js"/>"></script>
        <script src="<c:url value="/resources/js/calendar/fullcalendar.min.js"/>"></script>
        <script src="<c:url value="/resources/js/calendar/lang-all.js"/>"></script>
        <script src="<c:url value="/resources/js/calendar/bootstrapValidator.min.js"/>"></script>
        
        <script src="<c:url value="/resources/js/calendar/bootstrap-colorpicker.min.js"/>"></script>
        <script src="<c:url value="/resources/js/calendar/bootstrap-timepicker.min.js"/>"></script>
        <script src="<c:url value="/resources/js/calendar/main.js"/>"></script>
    </head>
    <body>
    	<script src="<c:url value="/resources/js/validation/jquery.validate.js"/>"></script>
		<script src="<c:url value="/resources/js/validation/util.validate.js"/>"></script>
		
		<script type="text/javascript">
		
		$(document).ready( function() {
			  $("#crud-form").validate({
			    // Define as regras
			    rules:{
			      title:{
			        // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
			        required: true
			      },
			      time:{
			        // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
			        required: true
			      },
			      EndTime:{
				       
				        required: true
				      },
			      description:{
			        required: true
			      },

			     
			      
			      
			    },
			    // Define as mensagens de erro para cada regra
			    messages:{
			      title:{
			        required: "Digite um título",
			      },
			     time:{
			        required: "Escolha uma data inicial",
			      },
			      EndTime:{
			        required: "Escolha uma data final",
			      },
			      description:{
				        required: "Digite uma descrição",
				      },
   
			     
			    }
			  });
			});
		
		</script>
    	<input type="hidden" name="token" id="idtoken" value="${_csrf.token}"/>
            <div class="row_calendar clearfix">
                <div class="column">
                        <div id='calendar'></div>
                </div>
            </div>
        <div class="modal fade" id="modalCalendar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="error"></div>
                        <form class="form-horizontal" id="crud-form">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="title">Titulo:</label>
                                <div class="col-md-4">
                                    <input id="title" name="title" type="text" class="form-control input-md" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="time">Horario Inicial:</label>
                                <div class="col-md-4 input-append bootstrap-timepicker">
                                    <input id="time" name="time" type="time" class="form-control input-md" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="time">Horário final:</label>
                                <div class="col-md-4 input-append bootstrap-timepicker">
                                    <input id="idEndTime" name="EndTime" type="time" class="form-control input-md" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="description">Descrição:</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" id="description" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="color">Cor:</label>
                                <div class="col-md-4">
                                    <input id="color" name="color" type="color" class="form-control input-md" readonly="readonly" />
                                    <span class="help-block">Clique para escolher a cor.</span>
                                </div>
                            </div>
							
							<sec:authorize access="hasRole('ADMIN')">                            
	                            <div class="form-group">
	                                <label class="col-md-4 control-label" for="group1">Evento:</label>
	                                <div class="col-md-4">
	                                    <input type="radio" id="idgroup1" name="group1" value="Y"> Público<br>
										<input type="radio" id="idgroup1" name="group1" value="N" checked> Privado
	                                </div>
	                            </div>
                            </sec:authorize>
                        </form>
                    </div>
                    
                    <div class="modal-footer" id="modalFooterCalendar">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                    
                    <sec:authorize access="isAuthenticated()">
						<sec:authentication property="principal" var="userCalendar" />
						<input type="hidden" name="userRole" id="idUserRole" value="${userCalendar.authorities[0].authority}"/>
					</sec:authorize>
                </div>
            </div>
        </div>
    </body>
</html>