<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<customTag:templetePage bodyClass="" title="Cadastro de Clientes">
	<jsp:attribute name="extraScripts">
		<script>
    	   /* Executa a requisi��oo quando o campo CEP perder o foco */
    	   $('#idcep').blur(function(){
    	           /* Configura a requisi��oo AJAX */
	    	          // http:cep.correiocontrol.com.br/CEP.json
	    	         //  http:cep.republicavirtual.com.br/web_cep.php?formato=json&cep=05568090
    	           $.ajax({
    	                url : 'http://cep.correiocontrol.com.br/'+$('#idcep').val().replace("-","")+'.json', /* URL que ser�o chamada */ 
    	                type : 'GET', /* Tipo da requisi��o, nesse caso poderia ser usado POST*/ 
    	                data:'' , //Parametros da requisi��oo exemplo = 'cep=' + $('#cep').val() 
    	                dataType: 'json', /* Tipo de transmiss�o */
    	                success: function(data){
    	                    if(Object.keys(data).length >= 1){
    	                        $('#idrua').val(data.logradouro);
    	                        $('#idbairro').val(data.bairro);
    	                        $('#idcidade').val(data.localidade);
    	                        $('#idestado').val(data.uf);
    	                        $('#idnumero').focus();
    	                    }
    	                },
    	                error: function(XMLHttpRequest, textStatus, errorThrown){
    	                	alert("Cep n�o encontrado ou inv�lido, porfavor escreva o endereco manualmente.")
    	                    alert(textStatus);
    	                }
    	           });   
    	  	 return false;    
    	   });
		</script>
		<script src="<c:url value="/resources/js/validation/jquery.validate.js"/>"></script>
		<script src="<c:url value="/resources/js/validation/util.validate.js"/>"></script>
		
		<script type="text/javascript">
		
		$(document).ready( function() {
			  $("#formularioCliente").validate({
			    // Define as regras
			    rules:{
			      nome:{
			        // campoNome ser� obrigat�rio (required) e ter� tamanho m�nimo (minLength)
			        required: true, minlength: 5, maxlength:150
			      },
			      email:{
			        // campoEmail ser� obrigat�rio (required) e precisar� ser um e-mail v�lido (email)
			        required: true, email: true
			      },
			      numeroRegistro:{
				       
				        required: true
				      },
			      cnpj:{
			       cnpj:"both",
			        required: true
			      },
			      cpf:{
			    	  
				     cpf:"both",  
				 	required: true
				      },
				  iE:{
       
 					required: true
    			  },
				  iM:{
	       
					required: true
	 		     },
				 jucesp:{
		       
					required: true
			      },
				dataRegistro:{
			       
					required: true
			      },
				socioResponsavel:{
				       
					required: true
				      },
	
				"endereco.cep":{
						       
					required: true,
					maxlength: 9
									 },
				"endereco.bairro":{
										       
					required: true
									 },						 
				"endereco.estado":{
										       
					required: true
									 },
				"endereco.cidade":{
									       
					required: true
									  },
				 "endereco.rua":{
										       
					 required: true
					 				   },
				 "endereco.numero":{
										       
					 required: true
					 				   },
				"telefones.telefoneResidencial":{
					
					required:true
				},
				"telefones.telefoneComercial":{
					
					required:true
				},
				"telefones.telefoneCelular":{
					
					required:true
				}
			      
			      
			    },
			    // Define as mensagens de erro para cada regra
			    messages:{
			      nome:{
			        required: "Digite o seu nome",
			        minlength: "O seu nome deve conter, no m�nimo, 5 caracteres",
			        maxlength: "O seu nome deve conter no m�ximo 150 caracteres"
			      },
			      email:{
			        required: "Digite o seu e-mail para contato",
			        email: "Digite um e-mail v�lido"
			      },
			      numeroRegistro:{
			        required: "Digite o n�mero de registro",
			        minlength: "O n�mero de registro deve ter, no m�nimo, 5 caracteres"
			      },
			      cnpj:{
				        required: "Digite o n�mero de CNPJ",
				        cnpj:"CNPJ incorreto!",
				        minlength: "O CNPJ deve ter 14 caracteres"
				      },
			      cpf:{
				        required: "Digite o n�mero de CPF",
				        cpf:"CPF incompat�vel!",
				        minlength: "O CPF deve ter, no m�nimo 11 caracteres"
				      },
				   iE:{
					        required: "Digite o n�mero de Inscri��o Estadual",
					      },
				   iM:{
					        required: "Digite o n�mero de Inscri��o Municipal",

					      },
				   jucesp:{
						    required: "Digite o n�mero de JUCESP",
						      },
				   dataRegistro:{
							 required: "Insira uma data",
								      },
					socioResponsavel:{
							 required: "Insira um s�cio respons�vel",
										      },
					"endereco.cep":{
						
						required: "Insira o n�mero do seu CEP",
						maxlength: "O CEP deve ter 8 caracteres"
					
			      },
					"endereco.bairro":{
						
						required: "Insira o seu bairro"
					},
					
					"endereco.estado":{
						
						required: "Insira o seu estado"
					},
					
					"endereco.cidade":{
						
						required: "Insira a sua cidade"
					},
					"endereco.rua":{
						
						required: "Insira a sua rua"
					},
					"endereco.numero":{
						
						required: "Insira o n�mero de sua resid�ncia"
					},
					"telefones.telefoneResidencial":{
						
						required: "Insira o n�mero de telefone de sua resid�ncia"
					},
					"telefones.telefoneComercial":{
						
						required: "Insira o n�mero de telefone comercial"
					},
					"telefones.telefoneCelular":{
						
						required: "Insira o n�mero do seu celular"
					}
			    }			
											      

														      
				      

			  });
			});
		
		</script>
		
		
		
		<script src="<c:url value="/resources/js/maskPlugin/jquery.mask.min.js"/>"></script>
		<script>
			$(document).ready(function(){
				  $('#idcnpj').mask('00.000.000/0000-00');
			});
			
			$(document).ready(function(){
				  $('#idcpf').mask('000.000.000-00');
			});
			
			$(document).ready(function(){
				  $('#idnumeroRegistro').mask('00.000.000-0');
			});
			
			$(document).ready(function(){
				  $('#iddataRegistro').mask('00/00/0000');
			});
			$(document).ready(function(){
				  $('#idcep').mask('00000-000');
			});
			$(document).ready(function(){
				  $('#idtelefoneResidencial').mask('(00)0000-0000');
			});
			$(document).ready(function(){
				  $('#idtelefoneComercial').mask('(00)0000-0000');
			});
			$(document).ready(function(){
				  $('#idtelefoneCelular').mask('(00)00000-0000');
			});
			$(document).ready(function(){
				  $('#idiE').mask('000.000.000.000');
			});
			$(document).ready(function(){
				  $('#idiM').mask('000.000.000.000');
			});
			
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
<h3>Cadastro de Clientes</h3>
<form:form servletRelativeAction="/cliente" commandName="cliente" id="formularioCliente">
	<div class="form-group">
	    <label for="nome">Nome do Cliente:</label>
	    <form:input type="text" cssClass="form-control" id="idnome" placeholder="Digite um usu�rio" path="nome"/>
	</div>
	<div class="form-group">
	    <label for="email">Email:</label>
	    <form:input type="email" cssClass="form-control" id="idemail" placeholder="Digite um Email" path="email"/>
	</div>
	<div class="form-group">
	    <label for="cnpj">CNPJ:</label>
	    <form:input type="text" cssClass="form-control" id="idcnpj" placeholder="Digite o CNPJ de sua empresa" path="cnpj"/>
	</div>
	<div class="form-group">
	    <label for="cpf">CPF:</label>
	    <form:input type="text" cssClass="form-control" id="idcpf" placeholder="Digite o seu CPF" path="cpf"/>
	</div>
	<div class="form-group">
	    <label for="iE">Inscri��o Estadual:</label>&nbsp;<input type="checkbox" id="isencao" value="1"> Isento
	    <form:input type="text" cssClass="form-control" id="idiE" placeholder="Digite a inscri��o estadual" path="iE"/>
	</div>
	<div class="form-group">
	    <label for="iM">Inscri��o Municipal:</label>
	    <form:input type="text" cssClass="form-control" id="idiM" placeholder="Digite a inscri��o municipal" path="iM"/>
	</div>
	<div class="form-group">
	    <label for="jucesp">Jucesp ou outro org�o:</label>
	    <form:input type="text" cssClass="form-control" id="idjucesp" path="jucesp"/>
	</div>
	<div class="form-group">
	    <label for="numeroRegistro">N�mero de Registro:</label>
	    <form:input type="text" cssClass="form-control" id="idnumeroRegistro" placeholder="Digite o n�mero de registro" path="numeroRegistro"/>
	</div>
	<div class="form-group">
	    <label for="dataRegistro">Data de Registro:</label>
	    <form:input type="date" cssClass="form-control" id="iddataRegistro" placeholder="Digite a data do seu registro" path="dataRegistro"/>
	</div>
	<div class="form-group">
	    <label for="socioResponsavel">S�cio Respons�vel:</label>
	    <form:input type="text" cssClass="form-control" id="idsocioResponsavel" placeholder="Digite o nome do s�cio respons�vel" path="socioResponsavel"/>
	</div>
	
	<br/>
	<h4>Endere�o</h4>
	
	<div class="form-group">
	    <label for="endereco.cep">CEP:</label>
	    <form:input type="text" cssClass="form-control" id="idcep" placeholder="Digite um CEP" path="endereco.cep"/>
	</div>
	
	<div class="form-group">
	    <label for="endereco.bairro">Bairro:</label>
	    <form:input type="text" cssClass="form-control" id="idbairro" placeholder="Digite o bairro" path="endereco.bairro"/>
	</div>
	
	<div class="form-group">
	    <label for="endereco.estado">Estado:</label>
	    <form:input type="text" cssClass="form-control" id="idestado" placeholder="Digite o Estado" path="endereco.estado"/>
	</div>
	<div class="form-group">
	    <label for="endereco.cidade">Cidade:</label>
	    <form:input type="text" cssClass="form-control" id="idcidade" placeholder="Digite um Endere�o" path="endereco.cidade"/>
	</div>
	<div class="form-group">
	    <label for="endereco.rua">Rua:</label>
	    <form:input type="text" cssClass="form-control" id="idrua" placeholder="Digite um Endere�o" path="endereco.rua"/>
	</div>
	<div class="form-group">
	    <label for="endereco.numero">Numero:</label>
	    <form:input type="text" cssClass="form-control" id="idnumero" placeholder="Digite um Endere�o" path="endereco.numero"/>
	</div>
	<br/>
	
	<h4>Contato</h4>
	
	<div class="form-group">
	    <label for="telefones.telefoneResidencial">Telefone:</label>
	    <form:input type="text" cssClass="form-control" id="idtelefoneResidencial" placeholder="Digite um telefone" path="telefones.telefoneResidencial"/>
	</div>
	
	<div class="form-group">
	    <label for="telefoneComercial">Telefone Comercial:</label>
	    <form:input type="text" cssClass="form-control" id="idtelefoneComercial" placeholder="Digite um telefone comercial" path="telefones.telefoneComercial"/>
	</div>
	
	<div class="form-group">
	    <label for="telefoneCelular">Celular:</label>
	    <form:input type="text" cssClass="form-control" id="idtelefoneCelular" placeholder="Digite um celular" path="telefones.telefoneCelular"/>
	</div>
	
	<div class="input-group">
		<span class="button_left">
			<input type="submit" class="btn btn-primary" value="Cadastrar"/>
		</span>
	</div>
</form:form>
				</div>
			</div>
		</div>
		<script>
			$("#isencao").click(function() {
				if ($("#isencao").val() == 1) {
					$("#idiE").attr('disabled', 'disabled');
					$("#idiE").prop('disabled', true);
					$("#isencao").val(2);
				} else {
					$("#idiE").attr('disabled', 'enabled');
					$("#idiE").prop('disabled', false);
					$("#isencao").val(1);
				}
			})
		</script>
	</jsp:body>
		
</customTag:templetePage>