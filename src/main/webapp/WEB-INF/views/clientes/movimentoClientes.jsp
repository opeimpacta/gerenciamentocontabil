<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usu�rios">
	<jsp:attribute name="extraScripts">
		<script>

		</script>
	</jsp:attribute>
	<jsp:body>
			<div class="col-md-8">
		  <div class="middle_row">
  <div class="middle_row_content">
  <h3>Movimento Clientes</h3>
  <table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>Data de Recebimento</th>
        <th>Data de Pagamento</th>
        <th>Cliente</th>
		<th>Tipo</th>
        <th>D�bito</th>
        <th>Cr�dito</th>
        <th>Saldo</th>
		
		
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>01/07/2015</td>
        <td>05/07/2015</td>
        <td>ANEIS</td>
		<td>FGTS</td>
		<td>R$ 500,00</td>
		<td>R$ 500,00</td>
		<td>0</td>
		
      </tr>
      <tr>
        <td>01/07/2015</td>
        <td>05/07/2015</td>
        <td>VIA STAR</td>
		<td>ICMS</td>
		<td>R$ 500,00</td>
		<td>R$ 500,00</td>
		<td>0</td>

      </tr>
      <tr>
        <td>01/07/2015</td>
        <td>05/07/2015</td>
        <td>PALACIOl</td>
		<td>INSS</td>
		<td>R$ 500,00</td>
		<td>R$ 500,00</td>
		<td>0</td>

      </tr>
    </tbody>
</table>
<br/><br/>
<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <td>Soma</td>
        <td>R$2700,00</td>
        <td>R$12.500</td>
        <td>&nbsp;</td>
        <td>R$9700,00</td>
		</tr>
    </thead>
    <tbody>
    </tbody>
</table>

  </div>
  </div>
  
  </div>
	</jsp:body>
		
</customTag:templetePage>