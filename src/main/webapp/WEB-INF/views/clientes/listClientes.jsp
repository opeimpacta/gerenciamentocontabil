<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<customTag:templetePage bodyClass="" title="Gerenciamento de Clientes">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		
		<script>
			$(document).ready(function() {
				$('#idTbListaClientes').dataTable({
					"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
					"sPaginationType" : "full_numbers",
					"aoColumnDefs" : [ {
						bSortable : false,
						aTargets : [ -1, -2, -3, -4, -5,-6,-7,-8]
					} ],
					"aoColumns" : [ 
					                {"bVisible" : false}, 
					                null,
					                null,
					                null,
					                null,
					                null,
					                null,
					                null]
				});
			});
		</script>
		<script>
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Gerenciamento de Clientes</h3>
					<form:form servletRelativeAction="/cliente">
						<div class="col-md-11 bottom-buffer5">
						    <div class="row col-md-11 bottom-buffer25">
						    	<a class="btn btn-success" href="<c:url value="/cliente/create"/>"><span class="glyphicon glyphicon-plus"></span>Adicionar Clientes</a>
					   		 </div>
					    </div>
						<div class="col-md-11 text-center">
							<table class="table compact nowrap stripe"id="idTbListaClientes">
								<thead>
									<tr>
										<td class="display-th"></td>
										<th class="display-th">Nome</th>
										<th class="display-th">Email</th>
										<th class="display-th">Cnpj</th>
										<th class="display-th">Socio Responsavel</th>
										<th class="display-th">Telefone Comercial</th>
										<th class="display-th"></th>
										<th class="display-th"></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${clientes}" var="cliente" varStatus="status">	
										<tr>
											<td>
												${cliente.id}
											</td>
											<td>
												${cliente.nome}
											</td>
											<td>
												${cliente.email}
											</td>
											<td>
												${cliente.cnpj}
											</td>
											<td>
												${cliente.socioResponsavel}
											</td>

											<td>
												${cliente.telefones.telefoneComercial}
											</td>

											<td>
												<a class="btn btn-blue btn-xs" href="<c:url value="/cliente/find/${cliente.id}"/>">Atualizar</a>
											</td>
											<td>
												<a class="btn btn-danger btn-xs" id="idDeletecliente" href="<c:url value="/cliente/delete/${cliente.id}"/>">Deletar</a>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</form:form>
					</div>
				</div>
			</div>
	</jsp:body>

</customTag:templetePage>