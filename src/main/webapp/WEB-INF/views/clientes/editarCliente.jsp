<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<customTag:templetePage bodyClass="" title="Cadastro de Usu�rios">
	<jsp:attribute name="extraScripts">
		<script>
    	   /* Executa a requisi��oo quando o campo CEP perder o foco */
    	   $('#idcep').blur(function(){
    	           /* Configura a requisi��oo AJAX */
	    	          // http:cep.correiocontrol.com.br/CEP.json
	    	         //  http:cep.republicavirtual.com.br/web_cep.php?formato=json&cep=05568090
    	           $.ajax({
    	                url : 'http://cep.correiocontrol.com.br/'+$('#idcep').val()+'.json', /* URL que ser�o chamada */ 
    	                type : 'GET', /* Tipo da requisi��o, nesse caso poderia ser usado POST*/ 
    	                data:'' , //Parametros da requisi��oo exemplo = 'cep=' + $('#cep').val() 
    	                dataType: 'json', /* Tipo de transmiss�o */
    	                success: function(data){
    	                    if(Object.keys(data).length >= 1){
    	                        $('#idrua').val(data.logradouro);
    	                        $('#idbairro').val(data.bairro);
    	                        $('#idcidade').val(data.localidade);
    	                        $('#idestado').val(data.uf);
    	                        $('#idnumero').focus();
    	                    }
    	                },
    	                error: function(XMLHttpRequest, textStatus, errorThrown){
    	                    alert(textStatus);
    	                }
    	           });   
    	  	 return false;    
    	   });
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Editar Cliente</h3>
					<form:form servletRelativeAction="/cliente/update" commandName="cliente">
					<form:input type="hidden" value="${cliente.id}" name="id" path="id"/>
						<div class="form-group">
						    <label for="nome">Nome do Cliente:</label>
						    <form:input type="text" cssClass="form-control" id="idnome" placeholder="Digite um usu�rio" path="nome"/>
						</div>
						<div class="form-group">
						    <label for="email">Email:</label>
						    <form:input type="email" cssClass="form-control" id="idemail" placeholder="Digite um Email" path="email"/>
						</div>
						<div class="form-group">
						    <label for="cnpj">CNPJ:</label>
						    <form:input type="text" cssClass="form-control" id="idcnpj" placeholder="Digite o CNPJ de sua empresa" path="cnpj"/>
						</div>
						<div class="form-group">
						    <label for="cpf">CPF:</label>
						    <form:input type="text" cssClass="form-control" id="idcpf" placeholder="Digite o seu CPF" path="cpf"/>
						</div>
						<div class="form-group">
						    <label for="iE">Inscri��o Estadual:</label>
						    <form:input type="text" cssClass="form-control" id="idiE" placeholder="Digite a inscri��o estadual" path="iE"/>
						</div>
						<div class="form-group">
						    <label for="iM">Inscri��o Municipal:</label>
						    <form:input type="text" cssClass="form-control" id="idiM" placeholder="Digite a inscri��o municipal" path="iM"/>
						</div>
						<div class="form-group">
						    <label for="jucesp">Jucesp ou outro org�o:</label>
						    <form:input type="text" cssClass="form-control" id="idjucesp" path="jucesp"/>
						</div>
						<div class="form-group">
						    <label for="numeroRegistro">N�mero de Registro:</label>
						    <form:input type="text" cssClass="form-control" id="idnumeroRegistro" placeholder="Digite o n�mero de registro" path="numeroRegistro"/>
						</div>
						<div class="form-group">
						    <label for="dataRegistro">Data de Registro:</label>
						    <form:input type="date" cssClass="form-control" id="iddataRegistro" placeholder="Digite a data do seu registro" path="dataRegistro"/>
						</div>
						<div class="form-group">
						    <label for="socioResponsavel">S�cio Respons�vel:</label>
						    <form:input type="text" cssClass="form-control" id="idsocioResponsavel" placeholder="Digite o nome do s�cio respons�vel" path="socioResponsavel"/>
						</div>
						
						<br/>
						<h4>Endere�o</h4>
						
						<div class="form-group">
						    <label for="endereco.cep">CEP:</label>
						    <form:input type="text" cssClass="form-control" id="idcep" placeholder="Digite um CEP" path="endereco.cep"/>
						</div>
						
						<div class="form-group">
						    <label for="endereco.bairro">Bairro:</label>
						    <form:input type="text" cssClass="form-control" id="idbairro" placeholder="Digite o bairro" path="endereco.bairro"/>
						</div>
						
						<div class="form-group">
						    <label for="endereco.estado">Estado:</label>
						    <form:input type="text" cssClass="form-control" id="idestado" placeholder="Digite o Estado" path="endereco.estado"/>
						</div>
						<div class="form-group">
						    <label for="endereco.cidade">Cidade:</label>
						    <form:input type="text" cssClass="form-control" id="idcidade" placeholder="Digite um Endere�o" path="endereco.cidade"/>
						</div>
						<div class="form-group">
						    <label for="endereco.rua">Rua:</label>
						    <form:input type="text" cssClass="form-control" id="idrua" placeholder="Digite um Endere�o" path="endereco.rua"/>
						</div>
						<div class="form-group">
						    <label for="endereco.numero">Numero:</label>
						    <form:input type="text" cssClass="form-control" id="idnumero" placeholder="Digite um Endere�o" path="endereco.numero"/>
						</div>
						<br/>
						
						<h4>Contato</h4>
						
						<div class="form-group">
						    <label for="telefones.telefoneResidencial">Telefone:</label>
						    <form:input type="text" cssClass="form-control" id="idtelefoneResidencial" placeholder="Digite um telefone" path="telefones.telefoneResidencial"/>
						</div>
						
						<div class="form-group">
						    <label for="telefoneComercial">Telefone Comercial:</label>
						    <form:input type="text" cssClass="form-control" id="idtelefoneComercial" placeholder="Digite um telefone comercial" path="telefones.telefoneComercial"/>
						</div>
						
						<div class="form-group">
						    <label for="telefoneCelular">Celular:</label>
						    <form:input type="text" cssClass="form-control" id="idtelefoneCelular" placeholder="Digite um celular" path="telefones.telefoneCelular"/>
						</div>
						
						<div class="input-group">
							<span class="button_left">
								<input type="submit" class="btn btn-primary" value="Cadastrar"/>
							</span>
						</div>
						
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>