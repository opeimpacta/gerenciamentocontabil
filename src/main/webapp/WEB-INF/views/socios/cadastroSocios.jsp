<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<customTag:templetePage bodyClass="" title="Cadastro de Sócios">
	<jsp:attribute name="extraScripts">
	
	
			<script
			src="<c:url value="/resources/js/validation/jquery.validate.js"/>"></script>
		<script
			src="<c:url value="/resources/js/validation/util.validate.js"/>"></script>
		
		<script type="text/javascript">
			$(document)
					.ready(
							function() {
								$("#formularioSocio")
										.validate(
												{
													// Define as regras
													rules : {
														"user.name" : {
															// campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
															required : true,
															minlength : 5
														},
														"user.login" : {
															// campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
															required : true,
															email : true
														},

														cpf : {

															cpf : "both",
															required : true
														},

														"user.password" : {

															required : true
														},

														"user.passwordagain" : {

															required : true,
															equalTo : "#idpassword"
														},

														porcentagem : {

															required : true,
															max : 100
														},
														"telefones.telefoneComercial" : {

															required : true
														},

													},
													// Define as mensagens de erro para cada regra
													messages : {
														"user.name" : {
															required : "Digite o seu nome",
															minlength : "O seu nome deve conter, no mínimo, 5 caracteres"
														},
														"user.login" : {
															required : "Digite o seu e-mail para contato",
															email : "Digite um e-mail válido"
														},

														cpf : {
															required : "Digite o número de CPF",
															cpf : "CPF está errado!",
															minlength : "O CPF deve ter, no mínimo 11 caracteres",
															maxlength : "Digite no máximo 14 caracteres"
														},
														"user.password" : {
															required : "Digite uma senha",

														},
														"user.passwordagain" : {
															required : "Senha não equivalente",
															equalTo : "O campo confirmação de senha deve ser identico ao campo senha."

														},
														porcentagem : {
															required : "Digite uma porcentagem",
															max : "Por favor, digite um número entre 0 a 100"
														},
														"telefones.telefoneComercial" : {
															required : "Insira uma telefone comercial"
														},

													}
												});
							});
		</script>
	
	
	
	

	
	
	
	
	
		<script
			src="<c:url value="/resources/js/maskPlugin/jquery.mask.min.js"/>"></script>
		<script>
			$(document).ready(function() {
				$('#idtelefone').mask('(00)0000-0000');
			});
			$(document).ready(function() {
				$('#idcpf').mask('000.000.000-00');
			});
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row">
					<div class="middle_row_content">
						<h3>Cadastro de Sócios</h3>
						<form:form servletRelativeAction="/socio" id="formularioSocio">
							<div class="form-group">
								<label for="user.name">Nome do sócio:</label>
								<input type="text" class="form-control" id="idname"
									placeholder="Digite o nome do sócio" name="user.name">
							</div>
							<div class="form-group">
								<label for="user.login">Email:</label>
								<input type="email" class="form-control" id="idemail"
									placeholder="Digite o Email para o sócio" name="user.login">
							</div>
							<div class="form-group">
								<label for="cpf">CPF:</label>
								<input type="text" class="form-control" title="" id="idcpf"
									placeholder="Digite o CPF do sócio" name="cpf">
							</div>
							<div class="form-group">
								<label for="user.password">Senha:</label>
								<input type="password" class="form-control" id="idpassword"
									placeholder="Digite uma senha" name="user.password">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Digite a Senha Novamente:</label>
								<input type="password" class="form-control" id="senhanovamente"
									placeholder="Digite a senha novamente"
									name="user.passwordagain">
							</div>
							<div class="form-group">
								<label for="porcentagem">Porcentagem no lucro</label>
								<input type="number" class="form-control" id="idporcentagem"
									placeholder="Estipule a porcentagem de cada sócio"
									name="porcentagem">
							</div>
							<div class="form-group">
								<label for="telefones">Telefone</label>
								<input type="tel" class="form-control" id="idtelefone"
									name="telefones.telefoneComercial"
									placeholder="Digite o seu número de telefone" />
							</div>
							<div class="input-group">
								<span class="button_right">
									<input type="submit" class="btn btn-primary" value="Cadastrar" />
								</span>
							</div>
							 
							<input type="hidden" name="user.roles[0].role" value="ROLE_FUNC" />
						</form:form>
					</div>
				</div>
			</div>
		</div>
		<script>
			$("#idcpf").click(function() {
				if ($("#idcpf").val() == "CPF já está cadastrado") {
					$("#idcpf").val("").css({
						"color" : "#555",
						"border" : "1px solid #ccc"
					});

				}

			})
			$("#idcpf").blur(
					function() {
						var cpf = $("#idcpf").val();

						cpf = String(cpf.replace("-", ""));
						cpf = String(cpf.replace(".", ""));
						$.ajax({
							url : "/gerenciamentocontabil/socio/find-cpf/"
									+ String(cpf.replace(".", "")) + ".json",
							type : "GET",

							success : function(data) {
								if (data["user-cpf"].cpf == String(cpf.replace(
										".", ""))) {
									$("#idcpf").val("CPF já está cadastrado")
											.css("color", "#FF0000");
									$('#idcpf').css('border',
											'2px solid #FF0000');
									$('#idcpf').focus();
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								alert('Ajax não suportado no seu browser');
							}
						});
					});
			</script>
			<script>
			$("#idemail").click(function() {
				if ($("#idemail").val() == "Este email já está cadastrado") {
					$("#idemail").val("").css({
						"color" : "#555",
						"border" : "1px solid #ccc"
					});

				}

			})

			$("#idemail").blur(
					function() {
						var email = $("#idemail").val();
						
						console.log(email);

						$.ajax({
							url : "/gerenciamentocontabil/socio/find-email/"
									+ email + ".json",
							type : "GET",

							success : function(data) {
								console.log(email);
								console.log(data["socio-email"]);
								if (data["socio-email"].user.login == email) {
									$("#idemail").val(
											"Este email já está cadastrado")
											.css("color", "#FF0000");
									$('#idemail').css('border',
											'2px solid #FF0000');
									$('#idemail').focus();
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								alert('Ajax não suportado no seu browser');
							}
						});
					});
		</script>
	</jsp:body>

</customTag:templetePage>