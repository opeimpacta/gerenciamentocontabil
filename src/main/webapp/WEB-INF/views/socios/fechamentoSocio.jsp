<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usuários">
	<jsp:attribute name="extraScripts">
		<script>

		</script>
	</jsp:attribute>
	<jsp:body>
			<div class="col-md-8">
		   <div class="middle_row">
  <div class="middle_row_content">
  <h3>Fechamento Sócio</h3>
  
<form>  
	<div class="input-group">
      <div class="input-group-addon">Ano:</div>
      <select class="form-control">
	<option>2015</option>
	<option>2014</option>
	<option>2013</option>
	<option>2012</option>
	<option>2011</option>
	<option>2010</option>
	<option>2009</option>
	<option>2008</option>
    <option>2007</option>
	<option>2006</option>
	<option>2005</option>
	<option>2004</option>
	<option>2003</option>
	<option>2002</option>
	<option>2001</option>
	<option>2000</option>
	<option>1999</option>
	<option>1998</option>
	<option>1997</option>
	<option>1996</option>
	<option>1995</option>
	<option>1994</option>
	<option>1993</option>
	<option>1992</option>
	<option>1991</option>
    <option>1990</option>
	
  </select>
  </div>
  
 <div class="input-group">
      <div class="input-group-addon">Mês:</div>
      <select class="form-control">
  	<option>Janeiro</option>
	<option>Fevereiro</option>
	<option>Março</option>
	<option>Abril</option>
	<option>Maio</option>
	<option>Junho</option>
	<option>Julho</option>
	<option>Agosto</option>
	<option>Setembro</option>
	<option>Outubro</option>
	<option>Novembro</option>
	<option>Dezembro</option>

</select>
    </div>
    
    <div class="input-group">
      <div class="input-group-addon">Sócio:</div>
      <select class="form-control">
  	<option>X</option>
	<option>Y</option>
	<option>Z</option>


</select>
    </div>
 
    

</form>

<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>Data</th>
        <th>Histórico</th>
		<th>Valor</th>
		
		
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>22/09/2015</td>
        <td>x</td>
		<td>R$ 	23.650,00</td>

      </tr>
      
    </tbody>
	  </table>
      <table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>Soma:</th>
        <td>x</td>

		
		
      </tr>
    </thead>
    <tbody>

      
    </tbody>
  </table>

  </div>
  </div>
  
  </div>
	</jsp:body>
		
</customTag:templetePage>