<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usuários">
	<jsp:attribute name="extraScripts">
		<script>
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row">
					<div class="middle_row_content">
						<h3>Editar de Sócio</h3>
						<form:form servletRelativeAction="/socio/update" commandName="socio">
							<div class="form-group">
								<label for="user.name">Nome do sócio:</label>
								<form:input type="text" cssClass="form-control" id="idname" placeholder="Digite o nome do sócio" path="user.name"/>
							</div>
							<div class="form-group">
								<label for="user.login">Email:</label>
								<form:input type="email" cssClass="form-control" id="idemail" placeholder="Digite o Email para o sócio" path="user.login"/>
							</div>
							<div class="form-group">
								<label for="cpf">CPF:</label>
								<form:input type="text" cssClass="form-control" id="idcpf" placeholder="Digite o CPF do sócio" path="cpf"/>
							</div>
							<div class="form-group">
								<label for="user.password">Senha:</label>
								<form:password cssClass="form-control" id="idpassword" placeholder="Digite uma senha" path="user.password"/>
							</div>
							<div class="form-group">
								<label for="user.passwordConfirm">Digite a Senha Novamente:</label>
								<input type="password" class="form-control" id="idpasswordConfirm" placeholder="Digite uma senha" name="user.passwordConfirm"/>
							</div>
							<div class="form-group">
								<label for="porcentagem">Porcentagem no lucro</label>
								<form:input type="text" cssClass="form-control" id="idporcentagem" placeholder="Estipule a porcentagem de cada sócio" path="porcentagem"/>
							</div>
							<div class="form-group">
								<label for="telefones.telefoneComercial">Telefone</label>
								<form:input type="text" cssClass="form-control" id="idtelefone" path="telefones.telefoneComercial" />
							</div>
							<div class="input-group">
								<span class="button_right">
									<input type="submit" class="btn btn-primary" value="Cadastrar"/>
								</span>
							</div>
							<form:hidden path="id"/>
							<input type="hidden" name="user.roles[0].role" value="ROLE_FUNC"/>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>