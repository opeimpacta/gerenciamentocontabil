<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Gerenciamento de Sócios">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script>
			$(document).ready(function() {
				$('#idTbListaSocios').dataTable({
					"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
					"sPaginationType" : "full_numbers",
					"aoColumnDefs" : [ {
						bSortable : false,
						aTargets : [ -1, -2, -3, -4, -5,-6,-7,-8]
					} ],
					"aoColumns" : [ 
					                {"bVisible" : false}, 
					                null, 
					                null, 
					                null,
					                null,
					                null,
					                null,
					                null]
				});
			});
		</script>
		<script>
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Gerenciamento de Socios</h3>
					<form:form servletRelativeAction="/socio">
						<div class="row col-md-11 bottom-buffer25">

						    	<a class="btn btn-success" href="<c:url value="/socio/create"/>"><span class="glyphicon glyphicon-plus"></span>Adicionar Sócio</a>
					    </div>
							<div class="row">
								<div class="col-md-11 text-center">
									<table class="table compact nowrap stripe"id="idTbListaSocios">
										<thead>
											<tr>
												<td class="display-th"></td>
												<th class="display-th">Nome</th>
												<th class="display-th">Login</th>
												<th class="display-th">Telefone</th>
												<th class="display-th">Porcentagem no lucro</th>
												<th class="display-th">cpf</th>
												<th class="display-th"></th>
												<th class="display-th"></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${socios}" var="socio" varStatus="status">	
												<tr>
													<td>
														${socio.id}
													</td>
													<td>
														${socio.user.name}
													</td>
													<td>
														${socio.user.login}
													</td>
													<td>
														${socio.telefones.telefoneComercial}
													</td>
													<td>
														${socio.porcentagem}
													</td>
													<td>
														${socio.cpf}
													</td>
													<td>
														<a class="btn btn-blue btn-xs" href="<c:url value="/socio/find/${socio.id}"/>">Atualizar</a>
													</td>
													<td>
														<a class="btn btn-danger btn-xs" id="idDeleteSocio" href="<c:url value="/socio/delete/${socio.id}"/>">Deletar</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>

</customTag:templetePage>