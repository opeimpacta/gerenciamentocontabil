<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Fechamento Geral">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script src="<c:url value="/resources/js/accounting/accounting.js"/>"></script>
		
		<script type="text/javascript">
		
		$(function(){
			$('#submitIdConsulta').on('click',function(){
			    $('#show_table').show(200); // aparece a div
			});
		});
		</script>
				<style>
		
		.show_table{
  			display: none;
		}
		
		</style>
		<script>
			var table = null;
			var table2 = null;
			
			
			
			$('#submitIdConsulta').on('click', function() {
				table = $('#idTbListaSocio').DataTable({
						"bSort": false,
						"destroy":true,
						"sDom" : '<"top">rt<"bottom"><"clear">',
						"sPaginationType" : "full_numbers",
						"ajax" :{
							 "url": '/gerenciamentocontabil/relatorio/list.json',
				             "type": 'POST',
				             "data": buildSearchData(),
				             "dataSrc":function dataSrc(json){
				            	 var relatorioSocio = json["relatorio_socio"];
				            	 var somaTotalSocio = json["soma_total_socio"];
				            	 var relatorioCliente = json["relatorio_cliente"];
				            	 
				            	 setSomaTotalSocio(somaTotalSocio);
				            	 setSomaTotalCliente(relatorioCliente);
				            	 
				            	 return relatorioSocio;
				             }
						},
						"aoColumns": [
						      {"mData": "socio"},
						      {"mData": "lucro_final"}, 
						]	
					});
			});
			
			$('#submitIdConsulta').on('click', function() {
				table2= $('#idTbListaBanco').DataTable({
						"bSort": false,
						"destroy":true,
						"sDom" : '<"top">rt<"bottom"><"clear">',
						"sPaginationType" : "full_numbers",
						"ajax" :{
							 "url": '/gerenciamentocontabil/relatorio/list.json',
				             "type": 'POST',
				             "data": buildSearchData(),
				             "dataSrc":function dataSrc(json){
				            	 var relatorioBanco = json["relatorio_banco"];
				            	 var somaTotalBanco = json["soma_total_banco"];
				            	 
				            	 setSomaTotalBanco(somaTotalBanco);
				            	 
				            	 return relatorioBanco;
				             }
						},
						"aoColumns": [
						      {"mData": "nome"},
						      {"mData": "soma"}, 
						]	
					});
			});
			
			function setSomaTotalCliente(value){
				$('#idNomeCliente').html(value.nome);
				$('#idSomaTotalCliente').html(value.soma);
			}
			
			function setSomaTotalSocio(value){
				$('#idSomaTotal').html(value);
			}
			
			
			function setSomaTotalBanco(value){
				$('#idSomaTotatolBanco').html(value);
			}
			
			function reload_table()
		    {
				if(table != null){
					table.ajax.reload(); //reload datatable ajax	
				}
				 
		    }
			
			function buildSearchData(){
				 var obj = {
			            "dateFrom": $("#from-date").val(),
			            "dateTo"  : $("#to-date").val(),
			            "_csrf" : $("#idtoken").val()
						};
				 console.log(obj);
				 return obj;
			}
		</script>
	</jsp:attribute>
	<jsp:body>
		<input type="hidden" name="token" id="idtoken" value="${_csrf.token}"/>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">

					<h3>Fechamento Geral</h3>
					
					<label>Periodo</label>
					<input type="date" name="dateFrom" id="from-date"/>
					<label>Até</label>
					<input type="date"name="dateTo" id="to-date"/>
					
				<button class="btn btn-primary" id="submitIdConsulta"> <span class="glyphicon glyphicon-search"> </span> Pesquisar</button>
					<button class="btn btn-primary" onclick="reload_table()"><span class="glyphicon glyphicon-refresh"></span> Atualizar</button>
				
				
				
				
				
				<div class="show_table" id="show_table">
				<h4>Lucro de Sócios</h4>
					<div class="row top-buffer25">
						<div class="col-md-8 text-center">
							<table class="table compact nowrap stripe"id="idTbListaSocio">
								<thead>
									<tr>
										<th class="display-th">Nome</th>
										<th class="display-th">Lucro liquido</th>
									</tr>
								</thead>
							</table>
						</div> 
					</div>
					
				
				<h4>Lucro de Clientes</h4>
				
					
					
					<div class="row top-buffer25">
						<div class="col-md-8 text-center">
							<table class="table compact nowrap stripe">
								<thead>
									<tr>
										<th class="display-th">Nome</th>
										<th class="display-th">Valor Cliente</th>
									</tr>
									
									
									<tr>
										<td id="idNomeCliente"></td>
										<td id="idSomaTotalCliente"></td>
									</tr>
									
								</thead>
							</table>
						</div>
					</div>
					
					<div class="row top-buffer25">
						<div class="col-md-8 text-center">
							<table class="table compact nowrap stripe">
								<thead>
									<tr>
										<td>Soma:</td>
										<td id="idSomaTotal"></td>
									</tr>
									

									
								</thead>
							</table>
						</div>
					</div>

					

					<h4>Lucro de Bancos</h4>
					
					
					<div class="row top-buffer25">
						<div class="col-md-8 text-center">
							<table class="table compact nowrap stripe"id="idTbListaBanco">
								<thead>
									<tr>
										<th class="display-th">Instituição/Outros</th>
										<th class="display-th">Saldo</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					
					
					
					
					
					<div class="row top-buffer25">
						<div class="col-md-8 text-center">
							<table class="table compact nowrap stripe">
								<thead>
									<tr>
										<td>Soma:</td>
										<td id="idSomaTotatolBanco"></td>
									</tr>
									

									
								</thead>
							</table>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	</jsp:body>
</customTag:templetePage>
