<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Movimento Banco">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script src="<c:url value="/resources/js/accounting/accounting.js"/>"></script>
		<script>
			var table = null;
			var save_method;
		
			$('#submitIdConsulta').on('click', function() {
				table = $('#idTbListaMBanco').DataTable({
						"bSort": false,
						"bProcessing": false,
						"destroy":true,
						"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
						"sPaginationType" : "full_numbers",
						"ajax" :{
							 "url": '/gerenciamentocontabil/mbanco/list.json',
				             "type": 'POST',
				             "data": buildSearchData(),
				             "dataSrc":function dataSrc(json){
				            	 var json2 = json["mBanco"];
				            	 for(var key in json2)
				 			     {
				            		 json2[key].editInput = '<button class="btn btn-sm btn-primary" title="Editar" onclick="edit_movimento('+json2[key].id+')">'
				            							   +'<i class="glyphicon glyphicon-pencil"></i>'
				            							   +'Editar</button>'
				            							   +'<button class="btn btn-sm btn-danger" title="Deletar" onclick="delete_movimento('+json2[key].id+')">'
				            							   +'<i class="glyphicon glyphicon-trash"></i>'
				            							   +'Deletar</button>';
				            		 console.log(json2[key]);
				 			     }
							     return json2;
				             }
						},
						"aoColumns": [
						      {"mData": "data"},
						      {"mData": "historico"}, 
						      {"mData": "banco.nomeDoBanco"},
						      {"mData": "entrada"}, 
						      {"mData": "saida"},
						      {"mData": "editInput"}
						      
						],
						"footerCallback": function ( row, data, start, end, display ) {
							
							var somaColEntrada = totalSomaMovimento( 3, data, this.api());
							
							var somalColSaida = totalSomaMovimento( 4, data , this.api());
							
							var totalEntradaSaida = somaColEntrada - somalColSaida;
							
							console.log('----------Entrada--------->'+somaColEntrada)
							console.log('----------Saida--------->'+somalColSaida)
							console.log('----------Entrada e Saida--------->'+totalEntradaSaida)
							
							setVal(somaColEntrada, somalColSaida, totalEntradaSaida);
							
				        }
							
					});
			});
			
			function setVal(entrada, saida, entradaSaida){
				entrada = accounting.formatMoney(entrada+'0', "R$", 0);
				saida = accounting.formatMoney(saida+'0', "R$", 0);
				entradaSaida = accounting.formatMoney(entradaSaida+'0', "R$", 0);
				
				$('#tdSomEntrada').html(entrada.substring(0, entrada.length - 1));
				
				$('#tdSomSaida').html(saida.substring(0, saida.length - 1));
				
				$('#tdSomEntradaSaida').html(entradaSaida.substring(0, entradaSaida.length - 1));
			}
			
			function totalSomaMovimento( col, data, apiParam){
				var api = apiParam, data;
				 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                	i.replace(/[\D]+/g,'')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 
	            // Total over all pages
	            total = apiParam
	                .column( col )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = apiParam
	                .column( col, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            return pageTotal;
			}
			
			function reload_table()
		    {
				if(table != null){
					table.ajax.reload(); //reload datatable ajax	
				}
				 
		    }
			
		</script>
		<script>
			function buildSearchData(){
				 var obj = {
			            "dateFrom": $("#from-date").val(),
			            "dateTo"  : $("#to-date").val(),
			            "_csrf" : $("#idtoken").val()
						};
				 console.log(obj);
				 return obj;
			}
		</script>
		<script>
			function add_movimento()
		    {
			      save_method = 'add';
			      
			      $('#form')[0].reset(); // reset form on modals
			      $("#idForm").val('');
			      
			      $('#modal_form').modal('show'); // show bootstrap modal
			      $('.modal-title').text('Adicionar Movimento'); // Set Title to Bootstrap modal title
		    }
			
			function edit_movimento(id)
		    {
		      save_method = 'update';
		      $('#form')[0].reset(); // reset form on modals

		      //Ajax Load data from ajax
		      $.ajax({
		        url : "/gerenciamentocontabil/mbanco/find/" + id,
		        type: "GET",
		        dataType: 'json',
		        
		        success: function(data)
		        {
		           var data2 = data["mBanco"];
		           console.log(data2);
		            $('[name="id"]').val(data2.id);
		            $('[name="historico"]').val(data2.historico);
		            $('[name="cliente"]').html('<option value"'+data2.banco.id+'">'+data2.banco.nome+'</option>');
		            $('[name="entrada"]').val(data2.entrada);
		            $('[name="saida"]').val(data2.saida);
		            $('#idDataMovimento').val(data2.data);
		            
		            console.log(data2.data);
		            
		            
		            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
		            $('.modal-title').text('Editar Movimento'); // Set title to Bootstrap modal title
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		    }
			
			function save()
		    {
		      var url;
		      if(save_method == 'add') 
		      {
		        url = "/gerenciamentocontabil/mbanco/save.json";
		      }
		      else
		      {
		        url = "/gerenciamentocontabil/mbanco/update.json";
		      }

		       // ajax adding data to database
		          $.ajax({
		            url : url,
		            type: "POST",
		            data: getFormData(),
		            cache :  false,
		            
		            success: function(data)
		            {
		               //if success close modal and reload ajax table
		               $('#modal_form').modal('hide');
		               reload_table();
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Error adding / update data');
		            }
		        });
		    }
			
			function delete_movimento(id)
		    {
		      if(confirm('Tem certeza que deseja apagar este movimento?'))
		      {
		        // ajax delete data to database
		          $.ajax({
		            url : "/gerenciamentocontabil/mbanco/delete/"+id,
		            type: "POST",
		            dataType: "JSON",
		            data : {"_csrf" : $("#idtoken").val()},
		            success: function(data)
		            {
		               //if success reload ajax table
		               $('#modal_form').modal('hide');
		               reload_table();
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Erro ao adicionar ou atualizar movimento.');
		            }
		        });
		         
		      }
		    }
		</script>
		<script>
			function getFormData(){
			    var unindexed_array = $('form').serializeArray();
			    var indexed_array = {};
	
			    $.map(unindexed_array, function(n, i){
			    	if(n['value'].indexOf('R$') > -1){
			        	indexed_array[n['name']] = accounting.unformat(n['value'], ",");
			    	}
			    	else
			    		indexed_array[n['name']] = n['value'];
			    });
				
			    indexed_array["_csrf"] = $("#idtoken").val();
			    console.log('-------Format Json------->');
			    console.log(indexed_array);
			    return indexed_array;
			}
		</script>
		<script>
			$('#selectBanco').on('click', function() {
				
				if ($('#selectBanco').find("option").size() == 1) {
				    var options = '';
	                $.ajax({
	                    type: 'GET',
	                    url: '/gerenciamentocontabil/banco/list',
	                    dataType: 'json',
	
	                    success : function(response){
	                    	console.log(response);
	                    	$("#selectCliente option").remove();
	                        $.each(response, function(key, data) {
	                        	$.each(data, function (index, data) {
	                        		options += '<option value="' + data.id + '">' + data.nomeDoBanco + '</option>';
	                        	})
	                        	$("#selectBanco").html(options);
	                        })
	                        
	                    }
	                })
	
	                .done(function() {
	                    console.log("success");
	                })
	                .fail(function() {
	                    console.log("error");
	                });
				} 
			});
		</script>
	</jsp:attribute>
	<jsp:body>
		<input type="hidden" name="token" id="idtoken" value="${_csrf.token}"/>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Movimentos Banco</h3>
					<label>Periodo</label>
					<input type="date" name="dateFrom" id="from-date"/>
					<label>Até</label>
					<input type="date"name="dateTo" id="to-date"/>
					
					<input class="btn"  type="button" id="submitIdConsulta" value="Pesquisar"/>
					<input class="btn"  type="button"  value="Atualizar" onclick="reload_table()"/>
					<div class="top-buffer25">
						<div class="row col-md-11 top-buffer25 bottom-buffer25">
						    <div class="text-left">
						    	<button class="btn btn-success" onclick="add_movimento()"><i class="glyphicon glyphicon-plus"></i> Adicionar Movimento</button>
						    </div>
					    </div>
						<div class="row">
							<div class="col-md-9 text-center">
								<table class="table compact nowrap stripe"id="idTbListaMBanco">
									<thead>
										<tr>
											<th class="display-th">Dia</th>
											<th class="display-th">Historico</th>
											<th class="display-th">Nome do Banco</th>
											<th class="display-th">Entrada</th>
											<th class="display-th">Saida</th>
											<th class="display-th" style="width:125px;">Ação</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					
					<div class="top-buffer25">
						<div class="row">
							<div class="col-md-8">
								<table class="table table-bordered">
								    <thead>
								      <tr>
								        <th></th>
								        <th></th>
								        <th></th>
								        <th></th>
								        <th class="success">Entrada</th>
								        <th class="danger">Saida</th>
								      </tr>
								    </thead>
								    <tbody>
								      <tr>
								        <td><strong>Soma Total:</strong></td>
								        <td></td>
								        <td></td>
								        <td></td>
								        <td id="tdSomEntrada" class="success"></td>
								        <td id="tdSomSaida" class="danger"></td>
								      </tr>
								      <tr>
								        <td><strong>Total Entrada/Saida:</strong></td>
								        <td></td>
								        <td></td>
								        <td></td>
								        <td></td>
								        <td class="danger" id="tdSomEntradaSaida"></td>
								      </tr>
								    </tbody>
							  </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<c:import url="/WEB-INF/views/movimentos/modalMBanco.jsp"/>
	</jsp:body>
</customTag:templetePage>
