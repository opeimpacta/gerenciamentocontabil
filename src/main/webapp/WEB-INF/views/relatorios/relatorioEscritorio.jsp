<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Fechamento Caixa">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script src="<c:url value="/resources/js/accounting/accounting.js"/>"></script>
		
		<script type="text/javascript">
		
		$(function(){
			$('#submitIdConsulta').on('click',function(){
			    $('#show_table').show(200); // aparece a div
			});
		});
		</script>
				<style>
		
		.show_table{
  			display: none;
		}
		
		</style>
		<script>
			var table = null;
		
			$('#submitIdConsulta').on('click', function() {
				table = $('#idTbListaSocio').DataTable({
						"bSort": false,
						"destroy":true,
						"sDom" : '<"top">rt<"bottom"><"clear">',
						"sPaginationType" : "full_numbers",
						"ajax" :{
							 "url": '/gerenciamentocontabil/relatorio/list.json',
				             "type": 'POST',
				             "data": buildSearchData(),
				             "dataSrc":function dataSrc(json){
				            	 var relatorioCaixa = json["relatorio_caixa"];
				            	 var relatorioSocio = json["relatorio_socio"];
				            	 
				            	 setRelatorioCaixa(relatorioCaixa);
				            	 
				            	 return relatorioSocio;
				             }
						},
						"aoColumns": [
						      {"mData": "socio"},
						      {"mData": "porcentagem"}, 
						      {"mData": "lucro"},
						      {"mData": "vale"}, 
						      {"mData": "lucro_final"}
						]	
					});
			});
			
			function setRelatorioCaixa(value){
				$('#idRelatorioEntrada').html(value.entrada);
				$('#idRelatorioSaida').html(value.saida);
				$('#idRelatorioLucro').html(value.soma);
			}
			
			function setSomaTotalCliente(value){
				$('#tdSomEntradaSaida').html(value);
			}
			
			function reload_table()
		    {
				if(table != null){
					table.ajax.reload(); //reload datatable ajax	
				}
				 
		    }
			
			function buildSearchData(){
				 var obj = {
			            "dateFrom": $("#from-date").val(),
			            "dateTo"  : $("#to-date").val(),
			            "_csrf" : $("#idtoken").val()
						};
				 console.log(obj);
				 return obj;
			}
		</script>
	</jsp:attribute>
	<jsp:body>
		<input type="hidden" name="token" id="idtoken" value="${_csrf.token}"/>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">

					<h3>Fechamento Caixa</h3>
					
					<label>Periodo</label>
					<input type="date" name="dateFrom" id="from-date"/>
					<label>Até</label>
					<input type="date"name="dateTo" id="to-date"/>
					
					<button class="btn btn-primary" id="submitIdConsulta"> <span class="glyphicon glyphicon-search"> </span> Pesquisar</button>
					<button class="btn btn-primary" onclick="reload_table()"><span class="glyphicon glyphicon-refresh"></span> Atualizar</button>
					
					
					
					<div class="show_table" id="show_table">
					<div class="col-md-5">
					<table class="table table-striped top-buffer25 text-center">
					    <tr>
					    <th class="text-center">Entrada</th>
					    <th class="text-center">Saída</th>
					    <th class="text-center">Saldo/Lucro</th>
					
 						<tr>
 							<td id="idRelatorioEntrada"></td>
 							<td id="idRelatorioSaida"></td>
 							<td id="idRelatorioLucro"></td>
 						</tr>
					</table>
					</div>

					<div class="top-buffer25">
							<table class="table compact nowrap stripe text-center"id="idTbListaSocio">
								<thead>
									<tr>
										<th class="display-th">Nome</th>
										<th class="display-th">Porcentagem</th>
										<th class="display-th">Lucro</th>
										<th class="display-th">Vale</th>
										<th class="display-th">Saldo a Receber</th>
									</tr>
								</thead>
							</table>
					</div>
				</div>	
				</div>
			</div>
		</div>
	</jsp:body>
</customTag:templetePage>
