<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<customTag:templetePage bodyClass="" title="Cadastro de Bancos">
	<jsp:attribute name="extraScripts">
	
	<script src="<c:url value="/resources/js/validation/jquery.validate.js"/>"></script>
		<script src="<c:url value="/resources/js/validation/util.validate.js"/>"></script>
		
		<script type="text/javascript">
		
		$(document).ready( function() {
			  $("#formularioBanco").validate({
			    // Define as regras
			    rules:{
			      nomeDoBanco:{
			        // campoNome ser� obrigat�rio (required) e ter� tamanho m�nimo (minLength)
			        required: true
			      },
			      codigoDoBanco:{
			        // campoEmail ser� obrigat�rio (required) e precisar� ser um e-mail v�lido (email)
			        required: true
			      },
			      agencia:{
				       
				        required: true
				      },
			      contaCorrente:{
			        required: true
			      },
			     
			      
			      
			    },
			    // Define as mensagens de erro para cada regra
			    messages:{
			      nomeDoBanco:{
			        required: "Digite o nome do seu banco",
			      },
			      codigoDoBanco:{
			        required: "Digite seu c�digo de banco",
			      },
			      agencia:{
			        required: "Digite o n�mero de sua ag�ncia",
			      },
			      contaCorrente:{
				        required: "Digite o n�mero de conta corrente",
				      },
			     
			    }
			  });
			});
		
		</script>
		

	
		<script src="<c:url value="/resources/js/maskPlugin/jquery.mask.min.js"/>"></script>
		<script>
// 			$(document).ready(function(){
// 				  $('#idcontaCorrente').mask('00000-0');
// 			});
			$(document).ready(function(){
				  $('#idagencia').mask('00000');
			});
			
			
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Cadastro de Bancos</h3>
					<form:form servletRelativeAction="/banco" commandName="banco" id="formularioBanco">
						<div class="form-group">
							<label for="nomeDoBanco">Nome do Banco</label>
							<form:input type="text" cssClass="form-control" id="idnomeDoBanco" placeholder="Digite o nome do banco" path="nomeDoBanco"/>
							<form:errors path="nomeDoBanco"/>
						</div>
						<div class="form-group">
							<label for="codigoDoBanco">C�digo do Banco:</label>
							<form:input type="text" cssClass="form-control" id="idcodigoDoBanco" placeholder="Digite o c�digo do banco" path="codigoDoBanco"/>
							<form:errors path="codigoDoBanco"/>
						</div>
						<div class="form-group">
							<label for="agencia">Ag�ncia:</label>
							<form:input type="text" cssClass="form-control" id="idagencia" placeholder="Digite a ag�ncia do seu banco" path="agencia"/>
							<form:errors path="agencia"/>
						</div>
						<div class="form-group">
							<label for="contaCorrente">Conta Corrente</label>
							<form:input type="text" cssClass="form-control" id="idcontaCorrente" placeholder="Digite a conta corrente" path="contaCorrente"/>
							<form:errors path="contaCorrente"/>
						</div>
						<div class="input-group">
							<span class="button_right">
								<input type="submit" class="btn btn-primary" value="Cadastrar"/>
							</span>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>