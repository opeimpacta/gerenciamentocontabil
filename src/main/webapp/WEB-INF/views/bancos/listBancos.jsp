<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Gerenciamento de Bancos">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script>
			$(document).ready(function() {
				$('#idTbListaBancos').dataTable({
					"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
					"sPaginationType" : "full_numbers",
					"aoColumnDefs" : [ {
						bSortable : false,
						aTargets : [ -1, -2, -3, -4, -5,-6,-7]
					} ],
					"aoColumns" : [ 
					                {"bVisible" : false}, 
					                null, 
					                null, 
					                null,
					                null,
					                null,
					                null]
				});
			});
		</script>
		<script>
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Gerenciamento de Bancos</h3>
					<form:form servletRelativeAction="/banco">
						<div class="row col-md-11 bottom-buffer25">

						    	<a class="btn btn-success" href="<c:url value="/banco/create"/>"><span class="glyphicon glyphicon-plus"></span>Adicionar Bancos</a>
					    </div>
								<div class="col-md-11 text-center">
								  <div class="row">
									<table class="table compact nowrap stripe"
										id="idTbListaBancos" style="width:100% !important">
										<thead>
											<tr>
												<td class="display-th"></td>
												<th class="display-th">Nome Do Banco</th>
												<th class="display-th">Agencia</th>
												<th class="display-th">Conta</th>
												<th class="display-th">Cod. Indentif. do Banco</th>
												<th class="display-th"></th>
												<th class="display-th"></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${bancos}" var="banco" varStatus="status">	
												<tr>
													<td>
														${banco.id}
													</td>
													<td>
														${banco.nomeDoBanco}
													</td>
													<td>
														${banco.agencia}
													</td>
													<td>
														${banco.contaCorrente}
													</td>
													<td>
														${banco.codigoDoBanco}
													</td>
													<td>
														<a class="btn btn-blue btn-xs" href="<c:url value="/banco/find/${banco.id}"/>">Atualizar</a>
													</td>
													<td>
														<a class="btn btn-danger btn-xs" id="idDeletarBanco" href="<c:url value="/banco/delete/${banco.id}"/>">Deletar</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								</div>	
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>

</customTag:templetePage>