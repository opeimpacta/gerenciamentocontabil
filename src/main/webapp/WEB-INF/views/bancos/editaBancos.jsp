<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usu�rios">
	<jsp:attribute name="extraScripts">
		<script>

		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Editar de Bancos</h3>
					<form:form servletRelativeAction="/banco/update" commandName="banco">
						<div class="form-group">
							<label for="nomeDoBanco">Nome do Banco</label>
							<form:input type="text" cssClass="form-control" id="idnomeDoBanco" placeholder="Digite o nome do banco" path="nomeDoBanco"/>
						</div>
						<div class="form-group">
							<label for="codigoDoBanco">C�digo do Banco:</label>
							<form:input type="text" cssClass="form-control" id="idcodigoDoBanco" placeholder="Digite o c�digo do banco" path="codigoDoBanco"/>
						</div>
						<div class="form-group">
							<label for="agencia">Ag�ncia:</label>
							<form:input type="text" cssClass="form-control" id="idagencia" placeholder="Digite a ag�ncia do seu banco" path="agencia"/>
						</div>
						<div class="form-group">
							<label for="contaCorrente">Conta Corrente</label>
							<form:input type="text" cssClass="form-control" id="idcontaCorrente" placeholder="Digite a conta corrente" path="contaCorrente"/>
						</div>
						<div class="input-group">
							<span class="button_right">
								<input type="submit" class="btn btn-primary" value="Cadastrar"/>
							</span>
						</div>
						<form:hidden path="id"/>
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>