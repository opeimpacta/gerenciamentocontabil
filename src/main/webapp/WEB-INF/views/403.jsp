<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Acesso Negado</title>
		<style>
			body{
				background-image:url(/gerenciamentocontabil/resources/img/403/bg_acessdenied.jpg);
				background-size:100% 100%;
				margin:0;
				padding:0;
			}
			.monitor{
			    width: 600px;
			    height: 462px;
			    margin: auto;
			    background-image: url(/gerenciamentocontabil/resources/img/403/monitor.png);
			    position: absolute;
			    z-index: 99;
				left:50%;
				margin-top:5px;
				margin-left:-300px;
			
			}
			
			.mesa{
			    background-color: #493e34;
			    height: 317px;
			    width: 100%;
			    position: relative;
			    top: 303px;
			}
			
			.teclado{
				width:400px;
				height:147px;
				background-image:url(/gerenciamentocontabil/resources/img/403/maopc.png);
				position:absolute;
				bottom:0;
				left:50%;
				margin-left:-200px;
			}
			
			.container_monitor{
				width:500px;
				margin:auto;
				margin-top:50px;
				font-family:Dense;
				color:#555454;
				font-size:27px;
				
			}
			.titulo_permissionfailed{
			    margin: 0;
			    padding: 0;
			    font-size: 40px;
			    float: left;
			    text-transform: uppercase;
				font-weight:normal;
				padding-left: 30px;
			    padding-top: 30px;	
				
				
				
			}
				
			
			.icon_permissionfailed{
			    width: 147px;
			    height: 147px;
			    background-image: url(/gerenciamentocontabil/resources/img/403/icon-denied.png);
			    float: left;
			
			}
			.text_under{
				padding-left:20px;
				float:left;
			}
			
			.admin{
				font-style:italic;
				color:#be0028;
			}
			.span_admin{
				float:right;
				font-size:30px;
			}
			.img-admin{
				padding-left:10px;
				float:right;
			}
			.text-voltar{
				float:left;
				clear:both;
			}
		</style>
		
	</head>
	<body>
		<div class="monitor">
	      <div class="container_monitor">
	    	<div class="icon_permissionfailed">
	        </div>
	        <h3 class="titulo_permissionfailed">
	        		Acesso negado - 403
	        </h3>
	        <p class="text_under">Você não tem permissão para acessar essa página<br/>
	        
	        <span class="span_admin">Contate o <strong class="admin">administrador</strong><img class="img-admin" src="<c:url value="/resources/img/403/icon-admin.png"/>"/></span></p>
	        
	        
	        <span class="text-voltar">
	        	<<<<< Voltar a página anterior
	        </span>
	      </div>  
	    </div>
	    <div class="mesa">
	    	<div class="teclado">
	        </div>
	    </div>
	</body>
</html>