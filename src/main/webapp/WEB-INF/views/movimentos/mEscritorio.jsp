<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Movimento Escrit�rio">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script src="<c:url value="/resources/js/accounting/accounting.js"/>"></script>
			
		<script src="<c:url value="/resources/js/jquery-maskmoney/jquery-maskmoney-v3.0.2.js"/>"></script>
		
		<script type="text/javascript">
			$(function(){
				$('#submitIdConsulta').on('click',function(){
				    $('#show_table').show(200); // aparece a div
				});
			});
		</script>
		

		
		
		<script>
			var table = null;
			var save_method;
			
			
		
			$('#submitIdConsulta').on('click', function() {
				table = $('#idTbListaMEscritorio').DataTable({
						"bSort": false,
						"bProcessing": false,
						"destroy":true,
						"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
						"sPaginationType" : "full_numbers",
						"ajax" :{
							 "url": '/gerenciamentocontabil/mescritorio/list.json',
				             "type": 'POST',
				             "data": buildSearchData(),
				             "dataSrc":function dataSrc(json){
				            	 var json2 = json["mEscritorioDao"];
				            	 for(var key in json2)
				 			     {
				            		 json2[key].editInput = '<button class="btn btn-sm btn-blue" title="Editar" onclick="edit_movimento('+json2[key].id+')">'
				            							   +'<i class="glyphicon glyphicon-pencil"></i>'
				            							   +'Editar</button>'
				            							   +'<button class="btn btn-sm btn-danger" title="Deletar" onclick="delete_movimento('+json2[key].id+')">'
				            							   +'<i class="glyphicon glyphicon-trash"></i>'
				            							   +'Deletar</button>';
				            		 console.log(json2[key]);
				 			     }
							     return json2;
				             }
						},
						"aoColumns": [
						      {"mData": "data"},
						      {"mData": "historico"}, 
						      {"mData": "cliente.nome"},
						      {"mData": "entrada"}, 
						      {"mData": "saida"},
						      {"mData": "editInput"}
						      
						],
						"footerCallback": function ( row, data, start, end, display ) {
							
							var somaColEntrada = totalSomaMovimento( 3, data, this.api());
							
							var somalColSaida = totalSomaMovimento( 4, data , this.api());
							
							var totalEntradaSaida = somaColEntrada - somalColSaida;
							
							console.log('----------Entrada--------->'+somaColEntrada)
							console.log('----------Saida--------->'+somalColSaida)
							console.log('----------Entrada e Saida--------->'+totalEntradaSaida)
							
							setVal(somaColEntrada, somalColSaida, totalEntradaSaida);
							
				        }
							
					});
			});
			
			function setVal(entrada, saida, entradaSaida){
				entrada = accounting.formatMoney(entrada+'0', "R$", 0);
				saida = accounting.formatMoney(saida+'0', "R$", 0);
				entradaSaida = accounting.formatMoney(entradaSaida+'0', "R$", 0);
				
				$('#tdSomEntrada').html(entrada.substring(0, entrada.length - 1));
				
				$('#tdSomSaida').html(saida.substring(0, saida.length - 1));
				
				$('#tdSomEntradaSaida').html(entradaSaida.substring(0, entradaSaida.length - 1));
			}
			
			function totalSomaMovimento( col, data, apiParam){
				var api = apiParam, data;
				 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                	i.replace(/[\D]+/g,'')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 
	            // Total over all pages
	            total = apiParam
	                .column( col )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = apiParam
	                .column( col, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            return pageTotal;
			}
			
			function reload_table()
		    {
				if(table != null){
					table.ajax.reload(); //reload datatable ajax	
				}
				 
		    }
			
		</script>
		
		<style>
			.show_table{
	  			display: none;
			}
		</style>
		
		<script>
			function buildSearchData(){
				 var obj = {
			            "dateFrom": $("#from-date").val(),
			            "dateTo"  : $("#to-date").val(),
			            "_csrf" : $("#idtoken").val()
						};
				 console.log(obj);
				 return obj;
			}
		</script>
		<script>
			function add_movimento()
		    {
			      save_method = 'add';
			      
			      $('#form')[0].reset(); // reset form on modals
			      $("#idForm").val('');
			      
			      $('#modal_form').modal('show'); // show bootstrap modal
			      $('.modal-title').text('Adicionar Movimento'); // Set Title to Bootstrap modal title
		    }
			
			function edit_movimento(id)
		    {
		      save_method = 'update';
		      $('#form')[0].reset(); // reset form on modals

		      //Ajax Load data from ajax
		      $.ajax({
		        url : "/gerenciamentocontabil/mescritorio/find/" + id,
		        type: "GET",
		        dataType: 'json',
		        
		        success: function(data)
		        {
		           var data2 = data["mEscritorio"];
		           console.log(data2);
		            $('[name="id"]').val(data2.id);
		            $('[name="historico"]').val(data2.historico);
		            $('[name="cliente"]').html('<option value"'+data2.cliente.id+'">'+data2.cliente.nome+'</option>');
		            $('[name="entrada"]').val(data2.entrada);
		            $('[name="saida"]').val(data2.saida);
		            $('#idDataMovimento').val(data2.data);
		            
		            console.log(data2.data);
		            
		            
		            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
		            $('.modal-title').text('Editar Movimento'); // Set title to Bootstrap modal title
		            
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error get data from ajax');
		        }
		    });
		    }
			
			function save()
			{
				if(validator(['idhistorico', 'selectCliente','idDataMovimento'])) {
						
					var url;
					if(save_method == 'add') 
					{
						url = "/gerenciamentocontabil/mescritorio/save.json";
					}
					else
					{
						url = "/gerenciamentocontabil/mescritorio/update.json";
					}
					// ajax adding data to database
					$.ajax({
						url : url,
						type: "POST",
						data: getFormData(),
						cache :  false,
						            
						success: function(data)
						{
						//if success close modal and reload ajax table
						$('#modal_form').modal('hide');
							reload_table();
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert('Error adding / update data');
						}
					});
				       
				}
			}
			
			function delete_movimento(id)
		    {
		      if(confirm('Tem certeza que deseja apagar este movimento?'))
		      {
		        // ajax delete data to database
		          $.ajax({
		            url : "/gerenciamentocontabil/mescritorio/delete/"+id,
		            type: "POST",
		            dataType: "JSON",
		            data : {"_csrf" : $("#idtoken").val()},
		            success: function(data)
		            {
		               //if success reload ajax table
		               $('#modal_form').modal('hide');
		               reload_table();
		            },
		            error: function (jqXHR, textStatus, errorThrown)
		            {
		                alert('Erro ao adicionar ou atualizar movimento.');
		            }
		        });
		         
		      }
		    }
		</script>
		<script>
			function getFormData(){
			    var unindexed_array = $('form').serializeArray();
			    var indexed_array = {};
	
			    $.map(unindexed_array, function(n, i){
			    	if(n['value'].indexOf('R$') > -1){
			        	indexed_array[n['name']] = accounting.unformat(n['value'], ",");
			    	}
			    	else
			    		indexed_array[n['name']] = n['value'];
			    });
				
			    indexed_array["_csrf"] = $("#idtoken").val();
			    console.log('-------Format Json------->');
			    console.log(indexed_array);
			    return indexed_array;
			}
		</script>
		<script>
			$('#selectCliente').on('click', function() {
				
				if ($('#selectCliente').find("option").size() == 1) {
				    var options = '';
	                $.ajax({
	                    type: 'GET',
	                    url: '/gerenciamentocontabil/cliente/list',
	                    dataType: 'json',
	
	                    success : function(response){
	                    	console.log(response);
	                    	$("#selectCliente option").remove();
	                        $.each(response, function(key, data) {
	                        	$.each(data, function (index, data) {
	                        		options += '<option value="' + data.id + '">' + data.nome + '</option>';
	                        	})
	                        	$("#selectCliente").html(options);
	                        })
	                        
	                    }
	                })
	
	                .done(function() {
	                    console.log("success");
	                })
	                .fail(function() {
	                    console.log("error");
	                });
				} 
			});
			
			
		    // Dead Basic Validation For Inputs
	        function validator(elements) {
	            var errors = 0;
	            var nameErrors;
	            $.each(elements, function(index, element){
	                if($.trim($('#' + element).val()) == '') {
	                	nameErrors =+ $('#' + element).attr('name');;
	                	errors++;
	                }
	            });
	            if(errors) {
	            	console.log(nameErrors);
	            	$('#errorModal').html('Por favor insira todos os campos.');
	                return false;
	            }
	            return true;
	        }
		</script>
		
		
		<script src="<c:url value="/resources/js/maskPlugin/jquery.mask.min.js"/>"></script>
		<script>
			$(document).ready(function(){
				  $('#idcnpj').mask('00.000.000/0000-00');
			});
			
		</script>
	</jsp:attribute>
	<jsp:body>
		<input type="hidden" name="token" id="idtoken" value="${_csrf.token}"/>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Movimentos Escrit�rio</h3>
					
						<div class="row col-md-11 top-buffer25 bottom-buffer25">
						    <div class="text-left">
						    	<button class="btn btn-success" onclick="add_movimento()"><i class="glyphicon glyphicon-plus"></i> Adicionar Movimento</button>
						    </div>
					    </div>
					    
					    <br/><br/><br/><br/>
					<label>Periodo</label>
					<input type="date" name="dateFrom" id="from-date"/>
					<label>At�</label>
					<input type="date"name="dateTo" id="to-date"/>
					
					<!--  <input class="btn"  type="button" id="submitIdConsulta" value="Pesquisar"/>
					<input class="btn"  type="button"  value="Atualizar" onclick="reload_table()"/>-->
					
					<button class="btn btn-primary" id="submitIdConsulta"> <span class="glyphicon glyphicon-search"> </span> Pesquisar</button>
					
				<div class="show_table" id="show_table">	
					<div class="top-buffer25">
					
						<div class="row">
							<div class="col-md-11 text-center">
								<table class="table compact nowrap stripe"id="idTbListaMEscritorio">
									<thead>
										<tr>
											<th class="display-th">Dia</th>
											<th class="display-th">Historio</th>
											<th class="display-th">Cliente</th>
											<th class="display-th">Entrada</th>
											<th class="display-th">Saida</th>
											<th class="display-th" style="width:125px;">A��o</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="top-buffer25">
						<div class="row">
							<div class="col-md-11">
								<table class="table table-bordered">
								    <thead>
								      <tr>
									<td colspan="9"></td>
								        <th class="success">Entrada</th>
								        <th class="danger">Saida</th>
								      </tr>
								    </thead>
								    <tbody>
								      <tr>
								        <td colspan="9"><strong>Soma Total:</strong></td>

								        <td id="tdSomEntrada" class="success"></td>
								        <td id="tdSomSaida" class="danger"></td>
								      </tr>
								      <tr>
								        <td colspan="10"><strong>Total Entrada/Saida:</strong></td>

								        <td class="danger" id="tdSomEntradaSaida"></td>
								      </tr>
								    </tbody>
							  </table>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
		<c:import url="/WEB-INF/views/movimentos/modalMEscritorio.jsp"/>
	</jsp:body>
</customTag:templetePage>
