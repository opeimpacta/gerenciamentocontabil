<!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Formulario Movimento</h3>
      </div>
      <div class="modal-body form">
      	<div class="row">
      		<div class="col-md-12 text-center text-red">
      			<span>Os campos marcados com (*), s�o obrigat�rios.</span>
      		</div>
      	</div>
      	<div class="text-red" id="errorModal"></div>
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" name="id" id="idForm"/> 
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Tipo de Hist�rico*:</label>
              <div class="col-md-8">
                <input name="historico" id="idhistorico" placeholder="Tipo de Lan�amento" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">S�cio*:</label>
              <div class="col-md-8">
                <select name="socio.id" class="form-control" id="selectSocio">
                  <option>Selecione o S�cio</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Valor*:</label>
              <div class="col-md-8">
                <input name="saida" id="idSaidaSocio" placeholder="Valor de Cr�dito" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Data de Cadastro*:</label>
              <div class="col-md-8">
                <input name="data" class="form-control" type="date" id="idDataMovimento">
              </div>
            </div>
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-blue">Salvar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  	<script type="text/javascript">
        $(document).ready(function(){
              $("#idSaidaSocio").maskMoney({prefix:'R$ ', decimal:",", thousands:"."});
        });
        
        $.ajax({
			url : "/gerenciamentocontabil/msocio/find-historico.json",
			type : "GET",
			success : function(data) {
				
				palavras= new Array();
				
				for(i = 0; i < data["hist"].length; i++){
					palavras[i] = data["hist"][i].historico;
				}
				$('#idhistorico').autocomplete({
					lookup: palavras,
				});
			},
			error : function(jqXHR,textStatus,errorThrown) {
				alert('Ajax n�o suportado no seu browser');
							}
			});
    </script>