<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<customTag:templetePage bodyClass="" title="Gerenciamento de Usuarios">
	<jsp:attribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>" />
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/resources/css/dataTable/displayTag-estilo.css"/>" />
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
		<script
			src="<c:url value="/resources/js/dataTable/jquery.dataTables.bootstrap.js"/>"></script>
		<script>
			$(document).ready(function() {
				$('#idTbListaUsuario').dataTable({
					"sDom" : '<"top"fl>rt<"bottom"ip><"clear">',
					"sPaginationType" : "full_numbers",
					"aoColumnDefs" : [ {
						bSortable : false,
						aTargets : [ -1, -2, -3, -4, -5]
					} ],
					"aoColumns" : [ 
					                {"bVisible" : false}, 
					                null, 
					                null, 
					                null,
					                null,
					                null]
				});
			});
		</script>
		<script>
		</script>
		
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Gerenciamento de Usuarios</h3>
					<form:form servletRelativeAction="/socio">
						<div class="row col-md-11 bottom-buffer25">

						    	<a class="btn btn-success" href="<c:url value="/user/create"/>"><span class="glyphicon glyphicon-plus"></span>Adicionar Usuário</a>
					    </div>
							
								<div class="col-md-11 text-center">
								<div class="row">
									<table class="table compact nowrap stripe"
										id="idTbListaUsuario">
										<thead>
											<tr>
												<td class="display-th"></td>
												<th class="display-th">Nome</th>
												<th class="display-th">Login</th>
												<th class="display-th">Perfil</th>
												<th class="display-th"></th>
												<th class="display-th"></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${users}" var="users" varStatus="status">	
												<tr>
													<td>
														${status.index}
													</td>
													<td>
														${users.name}
													</td>
													<td>
														${users.login}
													</td>
													<td>
														${users.roles[0].description}
													</td>
													<td>
														<a class="btn btn-blue btn-xs" href="<c:url value="/user/find/${users.login}."/>">Editar</a>
													</td>
													<td>
														<a class="btn btn-danger btn-xs" id="idDeleteUser" href="<c:url value="/user/delete/${users.login}."/>">Deletar</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>

					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>

</customTag:templetePage>