<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usuários">
	<jsp:attribute name="extraScripts">
		<script src="<c:url value="/resources/js/validation/jquery.validate.js"/>"></script>
		<script src="<c:url value="/resources/js/validation/util.validate.js"/>"></script>

		<script type="text/javascript">
		
		$(document).ready( function() {
			  $("#formularioUsuario").validate({
			    // Define as regras
			    rules:{
			      name:{
			        // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
			        required: true, minlength: 5
			      },
			      login:{
			        // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
			        required: true, email: true
			      },

			      password:{
			    	  
				 	required: true
				      },
				      
				  password2:{
       
 					required: true,
 					equalTo:"#password"
    			  },
    			  
  			     "#selectRole":{
  			    	 required:true
  			     },
			      
			      
			    },
			    // Define as mensagens de erro para cada regra
			    messages:{
			      name:{
			        required: "Digite o seu nome",
			        minlength: "O seu nome deve conter, no mínimo, 5 caracteres"
			      },
			      login:{
			        required: "Digite o seu e-mail para contato",
			        email: "Digite um e-mail válido"
			      },

			      password:{
				        required: "Digite sua senha"
				        
				      },
				   password2:{
					        required: "Digite sua senha",
					        equalTo:"O campo confirmação de senha deve ser identico ao campo senha"
					      },
				   "#selectRole":{      
				  
					   required:"Selecione um perfil para usuário"
																		      
				      
			    },
			    }

				  });
			  
			});
		
		</script>
	
	
	
		<script>
				$('#selectRole').on('click', function() {
					
					if ($('#selectRole').find("option").size() == 1) {
					    var options = '';
		                $.ajax({
		                    type: 'GET',
		                    url: '/gerenciamentocontabil/role/list',
		                    dataType: 'json',
	
		                    success : function(response){
		                    	$("#selectRole option").remove();
		                        $.each(response, function(key, data) {
		                        	$.each(data, function (index, data) {
		                        		options += '<option value="' + data.authority + '">' + data.description + '</option>';
		                        	})
		                        })
		                        
		                        $("#selectRole").html(options);
		                        
		                    }
		                })
	
		                .done(function() {
		                    console.log("success");
		                })
		                .fail(function() {
		                    console.log("error");
		                });
					} 
				});
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Cadastro de Usuarios</h3>
					<form:form servletRelativeAction="/user" id="formularioUsuario">
						<div class="form-group">
						    <label for="name">Nome:</label>
						    <input type="text" class="form-control" id="name" placeholder="Digite um usuário" name="name"/>
						</div>
					    <div class="form-group">
						    <label for="login">Email:</label>
						    <input type="email" class="form-control" id="login" placeholder="Digite um Email" name="login"/>
					    </div>
					    <div class="form-group">
						    <label for="password">Senha:</label>
						    <input type="password" class="form-control" id="password" placeholder="Digite uma senha" name="password"/>
					    </div>
					    <div class="form-group">
						    <label for="password2">Senha Novamente:</label>
						    <input type="password" class="form-control" id="password2" placeholder="Digite a senha novamente" name="password2"/>
					    </div>
					    
					    <div class="input-group">
					    	<div class="input-group-addon">Tipo de usuário:</div>
						    <select class="form-control" name="roles[0].role" id="selectRole" 	>
								<option>Selecione o Pefil do Usuário:</option>
							</select>
					    </div>  
						<div class="input-group">
							<span class="button_right">
								<input type="submit" class="btn btn-primary" value="Cadastrar"/>
							</span>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		<script>
			$("#login").click(function() {
				if ($("#login").val() == "Este email já está cadastrado") {
					$("#login").val("").css({
						"color" : "#555",
						"border" : "1px solid #ccc"
					});

				}

			})
			$("#login").blur(
					function() {
						var email = $("#login").val();

						$.ajax({
							url : "/gerenciamentocontabil/user/find-email/"
									+ email + ".json",
							type : "GET",

							success : function(data) {
								console.log(email);
								console.log(data["user-email"].login);
								if (data["user-email"].login == email) {
									$("#login").val("Este email já está cadastrado")
									.css("color", "#FF0000");
									$('#login').css('border',
											'2px solid #FF0000');
									$('#login').focus();
								}
							},
							error : function(jqXHR, textStatus, errorThrown) {
								alert('Ajax não suportado no seu browser');
							}
						});
					});
		</script>
	</jsp:body>
		
</customTag:templetePage>