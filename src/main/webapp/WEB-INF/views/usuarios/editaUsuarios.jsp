<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Atualização de Usuario">
	<jsp:attribute name="extraScripts">
		<script>
				$('#selectRole').on('click', function() {
					
					if ($('#selectRole').find("option").size() == 1) {
					    var options = '';
		                $.ajax({
		                    type: 'GET',
		                    url: '/gerenciamentocontabil/role/list',
		                    dataType: 'json',
	
		                    success : function(response){
		                    	$("#selectRole option").remove();
		                        $.each(response, function(key, data) {
		                        	$.each(data, function (index, data) {
		                        		options += '<option value="' + data.authority + '">' + data.description + '</option>';
		                        	})
		                        	$("#selectRole").html(options);
		                        })
		                        
		                    }
		                })
	
		                .done(function() {
		                    console.log("success");
		                })
		                .fail(function() {
		                    console.log("error");
		                });
					} 
				});
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Editar Usuarios</h3>
					<form:form servletRelativeAction="/user/update" commandName="socio">
						<div class="form-group">
						    <label for="name">Nome</label>
						    <input type="text" class="form-control" id="name" placeholder="Digite um usuário" name="name" value="${user.name}"/>
						</div>
					    <div class="form-group">
						    <label for="login">Email</label>
						    <input type="email" class="form-control" id="login" placeholder="Digite um Email" name="login" value="${user.login}"/>
					    </div>
					    <div class="form-group">
						    <label for="password">Senha</label>
						    <input type="password" class="form-control" id="password" placeholder="Digite uma senha" name="password" value="${user.password}"/>
					    </div>
					    <div class="form-group">
						    <label for="password2">Senha Novamente</label>
						    <input type="password" class="form-control" id="password2" placeholder="Digite a senha novamente" name="password2" value="${user.password}"/>
					    </div>
					    <div class="input-group">
					    	<div class="input-group-addon">Tipo de usuário</div>
						    <select class="form-control" name="roles[0].role" id="selectRole" 	>
								<option value="${user.roles[0].authority}">${user.roles[0].description}</option>
							</select>
					    </div>  
						<div class="input-group">
							<span class="button_left">
								<input type="submit" class="btn btn-primary" value="Cadastrar"/>
							</span>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>