<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<customTag:templetePage bodyClass="" title="Cadastro de Usuários">
	<jsp:attribute name="extraScripts">
		<script>
				$('#selectRole').on('click', function() {
					
					if ($('#selectRole').find("option").size() == 1) {
					    var options = '';
		                $.ajax({
		                    type: 'GET',
		                    url: '/gerenciamentocontabil/role/list',
		                    dataType: 'json',
	
		                    success : function(response){
		                    	$("#selectRole option").remove();
		                        $.each(response, function(key, data) {
		                        	$.each(data, function (index, data) {
		                        		options += '<option value="' + data.authority + '">' + data.description + '</option>';
		                        	})
		                        	$("#selectRole").html(options);
		                        })
		                        
		                    }
		                })
	
		                .done(function() {
		                    console.log("success");
		                })
		                .fail(function() {
		                    console.log("error");
		                });
					} 
				});
		</script>
	</jsp:attribute>
	<jsp:body>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
					<h3>Usuário não autorizado</h3>
					<img src="src/main/webapp/resources/img/denied.png"/>
					
				</div>
			</div>
		</div>
	</jsp:body>
		
</customTag:templetePage>