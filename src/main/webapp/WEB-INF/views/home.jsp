
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="customTag"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<style>
.box1 {
	width: 250px;
	height: 106px;
	float: left;
	margin-left: 0px;
	background-color: #bc2001;
	margin-bottom: 40px;
	margin-top: 40px;
	color: #FFF;
	cursor: pointer;
}

.box1:hover {
	-webkit-transition: .6s ease-in-out left;
	-o-transition: .6s ease-in-out left;
	transition: .6s ease-in-out left;
	background-color: rgba(188, 32, 1, 0.61);
}

.box2 {
	width: 250px;
	height: 98px;
	float: left;
	margin-left: 0px;
	background-color: #55bdf0;
	margin-bottom: 40px;
	margin-top: 40px;
	color: #FFF;
	cursor: pointer;
}

.box3 {
	width: 250px;
	height: 106px;
	float: left;
	margin-left: 40px;
	background-color: #81cd86;
	margin-bottom: 40px;
	margin-top: 40px;
	color: #FFF;
	cursor: pointer;
}

.box3:hover {
	-webkit-transition: .6s ease-in-out left;
	-o-transition: .6s ease-in-out left;
	transition: .6s ease-in-out left;
	background-color: rgba(129, 205, 134, 0.65);
}

.img_left_dashboard {
	width: 55px;
	height: 65px;
	float: left;
	background-image: url(/gerenciamentocontabil/resources/img/img_box.png);
	margin-left: 15px;
	margin-top: 27px;
}

.title_box_dashboard {
	width: 150px;
	float: left;
	margin-top: 25px;
	font-size: 21px;
	text-transform: uppercase;
	margin-left: 20px;
}

.text_under_dashboard {
	width: 150px;
	float: left;
	text-align: left;
	font-size: 20px;
	margin-left: 20px;
}

#movimentoEscritorio {
	display: none;
	clear: both;
}

#movimentoSocio {
	display: none;
	clear: both;
}

#movimentoCliente {
	/* display:none; */
	clear: both;
}
</style>

<customTag:templetePage bodyClass="" title="home">

	<jsp:body>
		<script>
			$(document).ready(function() {
				$("#movimentoEscritorio").hide();
				$("#movimentoCliente").hide();
			});
		</script>
		<div class="col-md-8">
			<div class="middle_row">
				<div class="middle_row_content">
				<h3>Seja bem vindo, 
				</h3>
				
				<div class="box1">
    	<div class="img_left_dashboard">
        </div>
    
    	<div class="title_box_dashboard">
    		Movimento Escritório
        </div>
        
        <div class="text_under_dashboard">
        Clique e veja a movimentação do escritório.
        </div>
    </div>

   	<div class="box3">
    <div class="img_left_dashboard">
        </div>
    
    	<div class="title_box_dashboard">
    		Movimento Cliente
        </div>
        
        <div class="text_under_dashboard">
        	 Clique e veja a movimentação geral dos clientes.
        </div>
    </div>
				
				
					<div id="movimentoEscritorio">
						<div style="width: 50%">
							<h1>Movimento Escritório</h1>
							<p>R$</p>
							<canvas id="canvas-escritorio" height="450" width="600"></canvas>
							<p>Legendas:</p>
							<div id="entrada">
							<p style="display: inline-block">Entrada R$ - </p> <div
									style="display: inline-block; width: 20px; height: 20px; background-color: rgba(151, 187, 205, 0.5); border: 2px solid rgba(151, 187, 205, 0.8);"></div>
							</div>
							<div id="saida">
							<p style="display: inline-block">Saída R$ - </p> <div
									style="display: inline-block; width: 20px; height: 20px; background-color: rgba(205, 187, 151, 0.5); border: 2px solid rgba(205, 187, 151, 0.8);"></div> 
							</div>
						</div>
			
						
						<script>
							// Ajax Load data from ajax
							$(".box1")
									.click(
											function() {
												$("#movimentoEscritorio").show("fast");
												$("#movimentoCliente").hide("fast");
												$.ajax({

															url : "/gerenciamentocontabil/relatoriodash/list-escritorio.json",
															type : "GET",
															dataType : 'json',

															success : function(
																	dados) {
																var list = dados["list-escritorio"];
																var entrada = [];
																var saida = [];
																var data = [];
																var barChartData = {};

																for (i = 0; i < list.length; i++) {
																	entrada[i] = list[i].entrada;
																	saida[i] = list[i].saida;
																	data[i] = list[i].data;

																}
																retorno = {
																	"retornando" : {
																		"entrada" : entrada,
																		"saida" : saida,
																		"data" : data
																	}
																};

																//////////////////////////////////////////
																//GRÁFICO
																///////////////////////////////////////////////////////////
																barChartData = {
																	labels : retorno["retornando"].data,
																	datasets : [
																			{
																				fillColor : "rgba(151,187,205,0.5)",
																				strokeColor : "rgba(151,187,205,0.8)",
																				highlightFill : "rgba(151,187,205,0.75)",
																				highlightStroke : "rgba(151,187,205,1)",
																				data : retorno["retornando"].entrada
																			},
																			{
																				fillColor : "rgba(205,187,151,0.5)",
																				strokeColor : "rgba(205,187,151,0.8)",
																				highlightFill : "rgba(205,187,151,0.75)",
																				highlightStroke : "rgba(205,187,151,1)",
																				data : retorno["retornando"].saida
																			} ]

																}

																var ctx = document
																		.getElementById(
																				"canvas-escritorio")
																		.getContext(
																				"2d");
																window.myBar = new Chart(
																		ctx)
																		.Bar(
																				barChartData,
																				{
																					responsive : true
																				})

																/////////////////////////////////////////

															},
															error : function(
																	jqXHR,
																	textStatus,
																	errorThrown) {
																alert('Erro ao pegar dados com ajax');
															}
														});
											});
						</script>
					</div>
					<div id="movimentoCliente">
						<div style="width: 50%">
							<h1>Movimento Cliente</h1>
							<p>R$</p>
							<canvas id="canvas-cliente" height="450" width="600"></canvas>
							<p>Legendas:</p>
							<div id="entrada">
							<p style="display: inline-block">Entrada R$ - </p> <div
									style="display: inline-block; width: 20px; height: 20px; background-color: rgba(151, 187, 205, 0.5); border: 2px solid rgba(151, 187, 205, 0.8);"></div>
							</div>
							<div id="saida">
							<p style="display: inline-block">Saída R$ - </p> <div
									style="display: inline-block; width: 20px; height: 20px; background-color: rgba(205, 187, 151, 0.5); border: 2px solid rgba(205, 187, 151, 0.8);"></div> 
							</div>
						</div>
			
						
						<script>
							// Ajax Load data from ajax
							$(".box3")
									.click(
											function() {
												$("#movimentoCliente").show("fast");
												$("#movimentoEscritorio").hide("fast");
												$.ajax({

															url : "/gerenciamentocontabil/relatoriodash/list-cliente.json",
															type : "GET",
															dataType : 'json',

															success : function(
																	dados) {
																var list = dados["list-cliente"];
																var entrada = [];
																var saida = [];
																var data = [];
																var barChartData = {};

																for (i = 0; i < list.length; i++) {
																	entrada[i] = list[i].entrada;
																	saida[i] = list[i].saida;
																	data[i] = list[i].data;

																}
																retorno = {
																	"retornando" : {
																		"entrada" : entrada,
																		"saida" : saida,
																		"data" : data
																	}
																};

																//////////////////////////////////////////
																//GRÁFICO
																///////////////////////////////////////////////////////////
																barChartData = {
																	labels : retorno["retornando"].data,
																	datasets : [
																			{
																				fillColor : "rgba(151,187,205,0.5)",
																				strokeColor : "rgba(151,187,205,0.8)",
																				highlightFill : "rgba(151,187,205,0.75)",
																				highlightStroke : "rgba(151,187,205,1)",
																				data : retorno["retornando"].entrada
																			},
																			{
																				fillColor : "rgba(205,187,151,0.5)",
																				strokeColor : "rgba(205,187,151,0.8)",
																				highlightFill : "rgba(205,187,151,0.75)",
																				highlightStroke : "rgba(205,187,151,1)",
																				data : retorno["retornando"].saida
																			} ]

																}

																var ctx = document
																		.getElementById(
																				"canvas-cliente")
																		.getContext(
																				"2d");
																window.myBar = new Chart(
																		ctx)
																		.Bar(
																				barChartData,
																				{
																					responsive : true
																				})

																/////////////////////////////////////////

															},
															error : function(
																	jqXHR,
																	textStatus,
																	errorThrown) {
																alert('Erro ao pegar dados com ajax');
															}
														});
											});
						</script>
	
					</div>
				</div>
			</div>
		</div>

		
	</jsp:body>
</customTag:templetePage>