$(function(){
	
    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    $('#color').colorpicker(); // Colopicker
//    $('#time').timepicker({
//        minuteStep: 5,
//        showInputs: false,
//        disableFocus: true,
//        showMeridian: false
//    });  // Timepicker
    // Fullcalendar
    $('#calendar').fullCalendar({
        
    	timeFormat: 'H(:mm)',
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month, basicWeek, basicDay'
        },
        
        lang:'pt-br',
        buttonText: {
		    today: 'Hoje',
		    month: 'Mês',
		    week: 'Semana',
		    day: 'Dia'
		},
		
        // Get all events stored in database
		events: {
            url : '/gerenciamentocontabil/calendar/list.json',
            success: function(data){
            	return data["events"];
            }
         },
        // Handle Day Click
        dayClick: function(date, event, view) {
        	currentDate = date.format('YYYY-MM-DD');
            // Open modal to add event
            modal({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-event', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Adicionar' // Buttons label
                    }
                },
                title: 'Adicionar Evento' +'('+ date.format('DD/MM/YYYY') + ')' // Modal title
            });
        },
        // Event Mouseover
        eventMouseover: function(calEvent, jsEvent, view){
            var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
            $("body").append(tooltip);
            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                    $('.event-tooltip').css('top', e.pageY + 10);
                    $('.event-tooltip').css('left', e.pageX + 20);
                });
        },
        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.event-tooltip').remove();
        },
        // Handle Existing Event Click
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;
            currentDate = calEvent.dateUpdate;
            
            currentUserRole = $('#idUserRole').val();
            
            // Open modal to edit or delete event
            
            modalUpdate({
                // Available buttons when editing
                buttons: {
                    Apagar: {
                        id: 'delete-event',
                        css: 'btn-danger',
                        label: 'Excluir'
                    },
                    update: {
                        id: 'update-event',
                        css: 'btn-success',
                        label: 'Atualizar'
                    }
                },
                title: 'Editar evento: ' + calEvent.title ,
                event: calEvent,
                userRole: currentUserRole
            });
            
        }
    });
    // Prepares the modal window according to data passed
    function modalUpdate(data) {
    	//reset form
    	$('#crud-form')[0].reset();
        // Set modal title
        $('.modal-title').html(data.title);
        // Clear buttons except Cancel
        $('#modalFooterCalendar button:not(".btn-default")').remove();
        // Set input values
        $('#title').val(data.event ? data.event.title : '');
        
        $('#description').val(data.event ? data.event.description : '');
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        
        if(data.userRole != "ROLE_ADMIN" && data.event.isGeneralEvent == "N"){
    		$.each(data.buttons, function(index, button){
	            $('#modalFooterCalendar').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
	        })
        }
        
        if(data.userRole == "ROLE_ADMIN"){
	        // Create Butttons
	        $.each(data.buttons, function(index, button){
	            $('#modalFooterCalendar').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
	        })
        }
        
        //Show Modal
        $('#modalCalendar').modal('show');
    }
    
    
 // Prepares the modal window according to data passed
    function modal(data) {
    	//reset form
    	$('#crud-form')[0].reset();
        // Set modal title
        $('.modal-title').html(data.title);
        // Clear buttons except Cancel
        $('#modalFooterCalendar button:not(".btn-default")').remove();
        // Set input values
        $('#title').val(data.event ? data.event.title : '');
        
        $('#description').val(data.event ? data.event.description : '');
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        
        // Create Butttons
        $.each(data.buttons, function(index, button){
            $('#modalFooterCalendar').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
        })
        
        //Show Modal
        $('#modalCalendar').modal('show');
    }
    // Handle Click on Add Button
    $('.modal').on('click', '#add-event',  function(e){
        if(validator(['title', 'description','time','idEndTime'])) {
            $.post('/gerenciamentocontabil/calendar', geraJson(), function(result){
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            });
        }
    });
    // Handle click on Update Button
    $('.modal').on('click', '#update-event',  function(e){
        if(validator(['title', 'description','time','idEndTime'])) {
            $.post('/gerenciamentocontabil/calendar/update', geraJsonUpdate(), function(result){
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            });
        }
    });
    // Handle Click on Delete Button
    $('.modal').on('click', '#delete-event',  function(e){
        $.get('/gerenciamentocontabil/calendar/delete/' + currentEvent._id, function(result){
            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
        });
    });
    // Get Formated Time From Timepicker
    function getTime() {
        var time = $('#time').val();
        return (time.indexOf(':') == 1 ? '0' + time : time) + ':00';
    }
    // Dead Basic Validation For Inputs
    function validator(elements) {
        var errors = 0;
        var nameErrors;
        $.each(elements, function(index, element){
            if($.trim($('#' + element).val()) == '') {
            	nameErrors =+ $('#' + element).attr('name');;
            	errors++;
            }
        });
        if(errors) {
        	console.log(nameErrors);
            $('.error').html('Por favor insira todos os campos.');
            return false;
        }
        return true;
    }
    
    
    function geraJson(){
    	
    	var obj={
            "title": $('#title').val(),
            "description": $('#description').val(),
            "color": $('#color').val(),
            "start": currentDate + ' ' + getTime(),
            "end": currentDate + ' ' + $('#idEndTime').val(),
            "dateUpdate": currentDate,
            "isGeneralEvent": getStatusEvento(),
            "_csrf" : $("#idtoken").val()
        }
    	
    	console.log(obj);
    	return obj;
    	
    }
    
    function geraJsonUpdate(){
    	var obj= {
                "id": currentEvent._id,
                "title": $('#title').val(),
                "description": $('#description').val(),
                "color": $('#color').val(),
                "start": currentDate + ' ' + getTime(),
                "end": currentDate + ' ' + $('#idEndTime').val(),
                "dateUpdate": currentDate,
                "isGeneralEvent": getStatusEvento(),
                "_csrf" : $("#idtoken").val()
            }
    	
    	console.log(obj);
    	return obj;
    }
    
    function getStatusEvento(){
    	var status = $("input[name='group1']:checked").val();
    	var resp = 'N'

    	if(status != null){
    		resp = status;
    	}
    	
    	console.log(resp);
    	return resp;
    }
 
});

