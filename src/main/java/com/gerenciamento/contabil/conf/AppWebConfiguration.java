package com.gerenciamento.contabil.conf;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.gerenciamento.contabil.controller.HomeController;
import com.gerenciamento.contabil.daos.ProductDAO;
import com.gerenciamento.contabil.service.RelatorioManager;
import com.gerenciamento.contabil.viewresolver.JsonViewResolver;

@EnableWebMvc
@ComponentScan(basePackageClasses={HomeController.class,ProductDAO.class,RelatorioManager.class}) //Está sendo setado as classes onde é onde pega somente o pacote para que possa ser feito o processamento dos annotation dentro dos pacotes.
public class AppWebConfiguration extends WebMvcConfigurerAdapter {
	
	@Bean
	public InternalResourceViewResolver InternalResourceViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	//Habilita a leitura de aquivos internos ao projeto para acesso externo do projeto, possibilitando a leitura de arquivos de script e css. Configurado na classe de security.
	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	
	//Adicionando Uso de ontras intepletaçoes via REST baseado na protocolo HTTP, abilitando response de json, html e xml através de um controller :
	//Onde também habilita a definição do retorno atravéz da url exemplo https//etc.json 
	@Bean
	public ViewResolver contentNegotiatingViewResolver(
			ContentNegotiationManager manager) {
		List<ViewResolver> resolvers = new ArrayList<ViewResolver>();
		
		resolvers.add(InternalResourceViewResolver());
		resolvers.add(new JsonViewResolver());
		
		ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
		resolver.setViewResolvers(resolvers);
		resolver.setContentNegotiationManager(manager);
		return resolver;
	}
	
	// Habilita o uso de requesições HTTP, utilizando o restTemplete encapsulado ao java.

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	//Adiciona 'N' conversores ao metodo configureMessageConverters da classe pai assim habilitando o uso de conversores 
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(converterJackson2Http());
        super.configureMessageConverters(converters);
    }
    
    //Permite a Conversão de json para o controller do spring mvc enviado através de post ou get
    @Bean
    public MappingJackson2HttpMessageConverter converterJackson2Http() {
    	MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    	return converter;
    }
    
    
//    //Redefine o estilo de conversão da annotation @DateTimeFormat, porque o date do formulario está no padrão 'mm-dd-yyyy' sendo que deve ser 'yyyy-mm-dd' para
//    //para formatação do spring
//    @Bean
//	public FormattingConversionService mvcConversionService() {
//		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService(false);
//		DateFormatterRegistrar registrar = new DateFormatterRegistrar();
//		registrar.setFormatter(new DateFormatter("yyyy-MM-dd"));
//		registrar.registerFormatters(conversionService);
//		return conversionService;
//	}
    
    //Indica o caminho do arquivo de properties de onde o spring mvc deve buscar as mensagem relativas com as chaves configuradas
    //O nome do bean deve ser exatamente messageSource, porque o spring procura exatamente esse nome para procurar as messagens
    @Bean(name="messageSource")
	public MessageSource loadBundle() {
		ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
		bundle.setBasename("/WEB-INF/messages");
		bundle.setDefaultEncoding("UTF-8");
		//indica o tempo que o arquivo de mensagem deve ser carregado não tendo a necessidade de reiniciar a aplicação ao altera-lo
		bundle.setCacheSeconds(60);
		return bundle;
	}
    
    //Configura a intenacionalização, mais relacionado ao message.properties dependendo do pais através do browser, permitindo fazer a leitura da menssagem correta 
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
	}
	
	@Bean
	public LocaleResolver localeResolver(){
		return new CookieLocaleResolver();
	}
	
}
