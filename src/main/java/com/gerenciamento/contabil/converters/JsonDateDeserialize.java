package com.gerenciamento.contabil.converters;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@Component
public class JsonDateDeserialize extends JsonDeserializer<Calendar>{
	
	
	
	@Override
	public Calendar deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String date = p.getText();
		Calendar cal = Calendar.getInstance();
        try {
        	cal.setTime(format.parse(date));
        	return cal;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
	}

}
