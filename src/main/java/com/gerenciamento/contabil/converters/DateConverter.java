package com.gerenciamento.contabil.converters;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class DateConverter {
	
	// Construtor privado para classes utilitárias.
	private DateConverter() {
	
	}
	
	public static java.sql.Date CalendarToDateSQL(Calendar cal){
		java.util.Date data = cal.getTime();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		String sData = formatter.format(data);

		return java.sql.Date.valueOf(sData);
	}
}
