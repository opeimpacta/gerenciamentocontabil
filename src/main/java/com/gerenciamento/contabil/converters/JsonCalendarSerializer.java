package com.gerenciamento.contabil.converters;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class JsonCalendarSerializer extends JsonSerializer<Calendar>{
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	

	@Override
	public void serialize(Calendar date, JsonGenerator gen,
			SerializerProvider provider)
					throws IOException, JsonProcessingException {
		
		String formattedDate = dateFormat.format(date.getTime());
		gen.writeString(formattedDate);
		
	}
}




