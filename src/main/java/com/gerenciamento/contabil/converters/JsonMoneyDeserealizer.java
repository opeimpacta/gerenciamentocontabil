package com.gerenciamento.contabil.converters;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@Component
public class JsonMoneyDeserealizer extends JsonDeserializer<BigDecimal> {
	
//	private static final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
	private static final DecimalFormat myFormatter = new DecimalFormat("R$ #,##0.00;-R$ #,##0.00");
	
	@Override
	public BigDecimal deserialize(JsonParser money, DeserializationContext context)
			throws IOException, JsonProcessingException {
		
//		String moneyValueString = money.getText();
//		Number moneyDouble = null;
//		
//		try {
//			moneyDouble = moneyFormat.parse(moneyValueString);
//		} catch (ParseException e) {
//			
//			e.printStackTrace();
//		}
//		
//		BigDecimal moneyValue = BigDecimal.valueOf(moneyDouble.doubleValue());
		
		myFormatter.setParseBigDecimal(true);
		String moneyValueString = money.getText();
		
		BigDecimal moneyDecimal = null;
		try {
			moneyDecimal = (BigDecimal) myFormatter.parse(moneyValueString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return moneyDecimal;
	}

}
