package com.gerenciamento.contabil.converters;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class JsonMoneySerializer extends JsonSerializer<BigDecimal>{
	
	private static final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
	
	@Override
	public void serialize(BigDecimal money, JsonGenerator gen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		
		String moneyValue = moneyFormat.format(money); // deve mostrar "R$ 00,00"  
		
		gen.writeString(moneyValue);
	}

}
