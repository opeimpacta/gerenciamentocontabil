package com.gerenciamento.contabil.service;

import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gerenciamento.contabil.daos.RelatorioDashDAO;
import com.gerenciamento.contabil.vo.RelatorioDashVO;

@Service
@Transactional
public class RelatorioDashManager {

	@Autowired
	RelatorioDashDAO relatorioDashDAO;

	public List<RelatorioDashVO> getDashEscritorio() {
		List<RelatorioDashVO> relatorioDashVO = this.relatorioDashDAO
				.getEntradaSaidaEscritorioPorMes(this.dataDozeMesesAtras());
		return relatorioDashVO;
	}

	public List<RelatorioDashVO> getDashSocio() {
		List<RelatorioDashVO> relatorioDashVO = this.relatorioDashDAO
				.getEntradaSaidaSocioPorMes(this.dataDozeMesesAtras());
		return relatorioDashVO;
	}

	public List<RelatorioDashVO> getDashCliente() {
		List<RelatorioDashVO> relatorioDashVO = this.relatorioDashDAO
				.getEntradaSaidaClientePorMes(this.dataDozeMesesAtras());
		return relatorioDashVO;
	}

	private Calendar dataDozeMesesAtras() {
		Calendar data = Calendar.getInstance();

		int mes = data.get(Calendar.MONTH) + 1;
		System.out.println(data.get(Calendar.YEAR) - 1);
		int ano = mes == 12 ? data.get(Calendar.YEAR)
				: data.get(Calendar.YEAR) - 1;

		data.set(Calendar.DAY_OF_MONTH, 1);
		data.set(Calendar.MONTH, mes);

		data.set(Calendar.YEAR, ano);

		int mes2 = data.get(Calendar.MONTH);
		int dia = data.get(Calendar.DAY_OF_MONTH);

		Calendar dataSaida = Calendar.getInstance();
		dataSaida.set(Calendar.YEAR, ano);
		dataSaida.set(Calendar.MONDAY, mes2);
		dataSaida.set(Calendar.DAY_OF_MONTH, dia);
		dataSaida.set(Calendar.HOUR_OF_DAY, 0);
		dataSaida.set(Calendar.MINUTE, 0);
		dataSaida.set(Calendar.SECOND, 0);
		return dataSaida;
	}
}
