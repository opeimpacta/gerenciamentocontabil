package com.gerenciamento.contabil.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gerenciamento.contabil.daos.RelatorioDAO;
import com.gerenciamento.contabil.daos.SocioDAO;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.util.Util;
import com.gerenciamento.contabil.vo.RelatorioBancoVO;
import com.gerenciamento.contabil.vo.RelatorioClienteVO;
import com.gerenciamento.contabil.vo.RelatorioSocioVO;
import com.gerenciamento.contabil.vo.RelatorioVO;

@Service
@Transactional
public class RelatorioManager {
	
	@Autowired
	RelatorioDAO relatorioDAO;
	
	@Autowired
	SocioDAO socioDAO;
	
	public RelatorioVO getRelatorioCaixa(DateBetween date) {
		RelatorioVO relatorioVO = relatorioDAO.getRelatorioCaixa(date);
		return relatorioVO;
	}
	
	public List<RelatorioSocioVO> getRelatorioSocio(DateBetween date) {
		List<RelatorioSocioVO> relatorioSocioVO = relatorioDAO.getRelatorioSocio(date);
		
		RelatorioVO relatorioVO = getRelatorioCaixa(date);
		
		relatorioSocioVO = Util.calcPorcentagemLucroSocio(relatorioSocioVO, relatorioVO);
		
		return relatorioSocioVO;
	}
	
	public RelatorioClienteVO getRelatorioCliente(DateBetween date) {
		
		RelatorioClienteVO relatorioClienteVO = relatorioDAO.getRelatorioCliente(date);
		
		return relatorioClienteVO;
	}
	
	public BigDecimal getSomaTotalSocio(RelatorioClienteVO relatorioClienteVO, List<RelatorioSocioVO> relatorioSocioVO) {
		
		BigDecimal soma_total_socio = new BigDecimal(0);
		
		for (int i = 0; i < relatorioSocioVO.size(); i++) {
			soma_total_socio = soma_total_socio.add(relatorioSocioVO.get(i).getLucro_final());
		}
		
		if(relatorioClienteVO.getSoma().longValueExact() >= 0){
			soma_total_socio = soma_total_socio.add(relatorioClienteVO.getSoma());
		}else{
			soma_total_socio = soma_total_socio.subtract(relatorioClienteVO.getSoma());
		}
		
		return soma_total_socio;
	}
	
	public List<RelatorioBancoVO> getRelatorioBanco(DateBetween date) {
		List<RelatorioBancoVO> relatorioBancoVO = relatorioDAO.getRelatorioBanco(date);
		
		return relatorioBancoVO;
	}
	
	public BigDecimal getSomaTotalBanco(List<RelatorioBancoVO> relatorioSocioVO) {
		
		BigDecimal soma_total_banco = new BigDecimal(0);
		
		for (int i = 0; i < relatorioSocioVO.size(); i++) {
			soma_total_banco = soma_total_banco.add(relatorioSocioVO.get(i).getSoma());
		}
		
		return soma_total_banco;
	}
	
	
	
}
