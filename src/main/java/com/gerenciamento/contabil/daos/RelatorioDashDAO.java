package com.gerenciamento.contabil.daos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.vo.RelatorioDashVO;

@Repository
public class RelatorioDashDAO {

	@PersistenceContext
	private EntityManager manager;

	public List<RelatorioDashVO> getEntradaSaidaEscritorioPorMes(Calendar data) {
		List<RelatorioDashVO> relatoriosDash = new ArrayList<>();

		Iterator<?> resultado = this.manager
				.createNamedQuery("MEscritorio.buscarConsolidadoPorMes")
				.setParameter("dataInicial", data).getResultList().iterator();

		while (resultado.hasNext()) {
			RelatorioDashVO relatorioDashVO = new RelatorioDashVO();
			Object[] coluna = (Object[]) resultado.next();

			relatorioDashVO.setData((String) coluna[0]);
			relatorioDashVO.setEntrada((BigDecimal) coluna[1]);
			relatorioDashVO.setSaida((BigDecimal) coluna[2]);

			relatoriosDash.add(relatorioDashVO);

		}

		return relatoriosDash;
	}

	public List<RelatorioDashVO> getEntradaSaidaSocioPorMes(Calendar data) {
		List<RelatorioDashVO> relatoriosDash = new ArrayList<>();

		Iterator<?> resultado = this.manager
				.createNamedQuery("MSocio.buscarConsolidadoPorMes")
				.setParameter("dataInicial", data).getResultList().iterator();

		while (resultado.hasNext()) {
			RelatorioDashVO relatorioDashVO = new RelatorioDashVO();
			Object[] coluna = (Object[]) resultado.next();

			relatorioDashVO.setData((String) coluna[0]);
			relatorioDashVO.setEntrada((BigDecimal) coluna[1]);
			relatorioDashVO.setSaida((BigDecimal) coluna[2]);

			relatoriosDash.add(relatorioDashVO);

		}

		return relatoriosDash;
	}

	public List<RelatorioDashVO> getEntradaSaidaClientePorMes(Calendar data) {
		List<RelatorioDashVO> relatoriosDash = new ArrayList<>();

		Iterator<?> resultado = this.manager
				.createNamedQuery("MCliente.buscarConsolidadoPorMes")
				.setParameter("dataInicial", data).getResultList().iterator();

		while (resultado.hasNext()) {
			RelatorioDashVO relatorioDashVO = new RelatorioDashVO();
			Object[] coluna = (Object[]) resultado.next();

			relatorioDashVO.setData((String) coluna[0]);
			relatorioDashVO.setEntrada((BigDecimal) coluna[1]);
			relatorioDashVO.setSaida((BigDecimal) coluna[2]);

			relatoriosDash.add(relatorioDashVO);

		}

		return relatoriosDash;
	}
}
