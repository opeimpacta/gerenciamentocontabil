package com.gerenciamento.contabil.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.Socio;
import com.gerenciamento.contabil.models.User;

@Repository
public class SocioDAO {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private UserDAO userDAO;

	public void save(Socio socio) {
		this.manager.persist(socio);
	}

	public List<Socio> list() {
		List<Socio> socios = this.manager
				.createQuery("from Socio", Socio.class).getResultList();
		
		return socios;

	}

	public void update(Socio socio) {
		this.manager.merge(socio);
	}

	public Socio findById(Long id) {
		return this.manager.find(Socio.class, id);
	}

	public Socio findByUser(User user) {
		Socio socio = (Socio) this.manager
				.createNamedQuery("Socio.buscarPorUsuario")
				.setParameter("user", user).getSingleResult();
		return socio;
	}

	public void delete(Socio socio) {
		this.manager.remove(socio);
	}
	
	public Socio findByCPF(String cpf){
		Socio socio = (Socio) this.manager
				.createNamedQuery("Socio.buscarPorCPF")
				.setParameter("cpf", cpf).getSingleResult();
		
		return socio;
	}
	
	public Socio findByEmail(String email){
		User user = this.userDAO.findByEmail(email);
		Socio socio = (Socio) this.manager
				.createNamedQuery("Socio.buscarPorUsuario")
				.setParameter("user", user).getSingleResult();
		System.out.println(socio);
		
		return socio;
	}
}