package com.gerenciamento.contabil.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.Cliente;

@Repository
public class ClienteDAO {

	@PersistenceContext
	private EntityManager manager;

	public void save(Cliente cliente) {
		this.manager.persist(cliente);
	}

	public void update(Cliente cliente) {
		this.manager.merge(cliente);
	}

	public void delete(Cliente cliente) {
		this.manager.remove(cliente);
	}

	public List<Cliente> list() {

		List<Cliente> clientes = this.manager.createQuery("from Cliente",
				Cliente.class).getResultList();
		return clientes;
	}

	public Cliente findById(Long id) {

		return this.manager.find(Cliente.class, id);
	}

	public Cliente findByCpf(String cpf) {
		cpf = cpf.replace(".", "").replace("-", "");
		Cliente cliente = this.manager
				.createNamedQuery("Cliente.buscaPorCpf", Cliente.class)
				.setParameter(":cpf", cpf).getSingleResult();
		return cliente;
	}
}
