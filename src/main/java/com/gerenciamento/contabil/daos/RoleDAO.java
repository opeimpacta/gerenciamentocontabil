package com.gerenciamento.contabil.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.Role;

@Repository
public class RoleDAO {
	
	@PersistenceContext
	EntityManager em;
	
	public List<Role> list(){
		String jpql ="from Role";
		
		List<Role> roles = em.createQuery(jpql, Role.class).getResultList();
		
		return roles;
	}
}
