package com.gerenciamento.contabil.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.Events;
import com.gerenciamento.contabil.models.User;
import com.gerenciamento.contabil.util.DateStartEnd;

@Repository
public class EventsDAO {

	@PersistenceContext
	private EntityManager em;
	
	public void save(Events events) {
		this.em.persist(events);
	}

	public void update(Events events) {
		this.em.merge(events);
	}

	public void delete(Events events) {
		this.em.remove(events);
	}

	public List<Events> list() {

		List<Events> events = this.em.createQuery("from Events", Events.class).getResultList();
		return events;
	}
	
	public List<Events> findByDate(DateStartEnd datas, User user) {
		List<Events> events = this.em
				.createNamedQuery("Events.buscarPorData",
						Events.class)
				.setParameter("dataInicial", datas.getStart())
				.setParameter("dataFinal", datas.getEnd())
				.setParameter("idUser", user.getLogin())
				.setParameter("eventoGeral", 'Y')
				.getResultList();
		
		return events;
	}

	public Events findById(Long id) {

		return this.em.find(Events.class, id);
	}

}
