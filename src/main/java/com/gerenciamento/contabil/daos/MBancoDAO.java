package com.gerenciamento.contabil.daos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.MBanco;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.vo.HistoricoAjaxVO;

@Repository
public class MBancoDAO {
	
	@PersistenceContext
	private EntityManager em;

	public void save(MBanco mBanco) {
		this.em.persist(mBanco);
	}

	public void update(MBanco mBanco) {
		mBanco = this.em.merge(mBanco);
	}

	public MBanco findById(Long id) {
		MBanco mBanco = this.em.find(MBanco.class, id);
		return mBanco;
	}

	public List<MBanco> getAll() {
		List<MBanco> mBanco = this.em.createQuery("from MBanco",
				MBanco.class).getResultList();
		return mBanco;
	}
	
	public void delete(MBanco mBanco) {
		this.em.remove(mBanco);
	}
	
	public List<MBanco> findByDate(DateBetween datas) {
		List<MBanco> mBancoLista = this.em
				.createNamedQuery("MBanco.buscarPorData",
						MBanco.class)
				.setParameter("dataInicial", datas.getDateFrom())
				.setParameter("dataFinal", datas.getDateTo()).getResultList();
		return mBancoLista;
	}
	
	public List<HistoricoAjaxVO> findHistoricoAjax() {
		List<HistoricoAjaxVO> historicos = new ArrayList<>();

		Iterator<?> resultado = this.em
				.createNamedQuery("MBanco.buscarHistoricoDistinto").getResultList()
				.iterator();

		while (resultado.hasNext()) {
			HistoricoAjaxVO historicoAjaxVO = new HistoricoAjaxVO();
			Object coluna = (Object) resultado.next();

			historicoAjaxVO.setHistorico((String) coluna);

			historicos.add(historicoAjaxVO);

		}

		return historicos;
	}

}
