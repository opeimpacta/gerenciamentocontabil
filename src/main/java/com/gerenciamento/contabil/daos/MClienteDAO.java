package com.gerenciamento.contabil.daos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.MCliente;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.vo.HistoricoAjaxVO;

@Repository
public class MClienteDAO {

	@PersistenceContext
	private EntityManager manager;

	public void save(MCliente mCliente) {
		this.manager.persist(mCliente);
	}

	public void update(MCliente mCliente) {
		mCliente = this.manager.merge(mCliente);
	}

	public MCliente findById(Long id) {
		MCliente mCliente = this.manager.find(MCliente.class, id);
		return mCliente;
	}

	public List<MCliente> getAll() {
		List<MCliente> mCliente = this.manager.createQuery("from MCliente",
				MCliente.class).getResultList();
		return mCliente;
	}

	public void delete(MCliente mCliente) {
		this.manager.remove(mCliente);
	}

	public List<MCliente> findByDate(DateBetween datas) {
		List<MCliente> mClienteLista = this.manager
				.createNamedQuery("MCliente.buscarPorData", MCliente.class)
				.setParameter("dataInicial", datas.getDateFrom())
				.setParameter("dataFinal", datas.getDateTo()).getResultList();
		return mClienteLista;
	}

	public List<HistoricoAjaxVO> findHistoricoAjax() {
		List<HistoricoAjaxVO> historicos = new ArrayList<>();

		Iterator<?> resultado = this.manager
				.createNamedQuery("MCliente.buscarHistoricoDistinto").getResultList()
				.iterator();

		while (resultado.hasNext()) {
			HistoricoAjaxVO historicoAjaxVO = new HistoricoAjaxVO();
			Object coluna = (Object) resultado.next();

			historicoAjaxVO.setHistorico((String) coluna);

			historicos.add(historicoAjaxVO);

		}

		return historicos;
	}
}
