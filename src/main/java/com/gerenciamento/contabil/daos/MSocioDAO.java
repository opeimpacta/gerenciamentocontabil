package com.gerenciamento.contabil.daos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.MSocio;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.vo.HistoricoAjaxVO;

@Repository
public class MSocioDAO {

	@PersistenceContext
	private EntityManager em;

	public void save(MSocio mSocio) {
		this.em.persist(mSocio);
	}

	public void update(MSocio mSocio) {
		mSocio = this.em.merge(mSocio);
	}

	public MSocio findById(Long id) {
		MSocio mSocio = this.em.find(MSocio.class, id);
		return mSocio;
	}

	public List<MSocio> getAll() {
		List<MSocio> mSocio = this.em.createQuery("from MSocio",
				MSocio.class).getResultList();
		return mSocio;
	}
	
	public void delete(MSocio mSocio) {
		this.em.remove(mSocio);
	}
	
	public List<MSocio> findByDate(DateBetween datas) {
		List<MSocio> mSocio = this.em
				.createNamedQuery("MSocio.buscarPorData",
						MSocio.class)
				.setParameter("dataInicial", datas.getDateFrom())
				.setParameter("dataFinal", datas.getDateTo()).getResultList();
		return mSocio;
	}
	
	public List<HistoricoAjaxVO> findHistoricoAjax() {
		List<HistoricoAjaxVO> historicos = new ArrayList<>();

		Iterator<?> resultado = this.em
				.createNamedQuery("MSocio.buscarHistoricoDistinto").getResultList()
				.iterator();

		while (resultado.hasNext()) {
			HistoricoAjaxVO historicoAjaxVO = new HistoricoAjaxVO();
			Object coluna = (Object) resultado.next();

			historicoAjaxVO.setHistorico((String) coluna);

			historicos.add(historicoAjaxVO);

		}

		return historicos;
	}

}
