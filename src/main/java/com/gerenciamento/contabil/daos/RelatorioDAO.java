package com.gerenciamento.contabil.daos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.vo.RelatorioBancoVO;
import com.gerenciamento.contabil.vo.RelatorioClienteVO;
import com.gerenciamento.contabil.vo.RelatorioSocioVO;
import com.gerenciamento.contabil.vo.RelatorioVO;

@Repository
public class RelatorioDAO {
	
	@PersistenceContext
	private EntityManager em;

	
	public List<RelatorioBancoVO> getRelatorioBanco(DateBetween date) {
		RelatorioBancoVO relatorioBancoVO;
		List <RelatorioBancoVO> relatorioBanco = new ArrayList<RelatorioBancoVO>();
		
		Iterator<?> rs = this.em.createNamedQuery("MBanco.relatorio")
				.setParameter("dataInicial", date.getDateFrom())
				.setParameter("dataFinal", date.getDateTo())
				.getResultList().iterator();
		
		while ( rs.hasNext() ) {
			
			relatorioBancoVO = new RelatorioBancoVO();
			Object[] row = (Object[]) rs.next(); 
			
			relatorioBancoVO.setNome((String) row[0]);
			relatorioBancoVO.setEntrada((BigDecimal) row[1]);
			relatorioBancoVO.setSaida((BigDecimal) row[2]);
			
			BigDecimal soma = relatorioBancoVO.getEntrada().subtract(relatorioBancoVO.getSaida());
			
			relatorioBancoVO.setSoma(soma);
			
			relatorioBanco.add(relatorioBancoVO);
		}
		
		return relatorioBanco;
	}
	
	public RelatorioClienteVO getRelatorioCliente(DateBetween date) {
		RelatorioClienteVO relatorioClienteVO = new RelatorioClienteVO();
		
		Iterator<?> rs = this.em.createNamedQuery("MCliente.relatorio")
				.setParameter("dataInicial", date.getDateFrom())
				.setParameter("dataFinal", date.getDateTo())				
				.getResultList().iterator();
		
		while ( rs.hasNext() ) {
			
			relatorioClienteVO = new RelatorioClienteVO();
			Object[] row = (Object[]) rs.next(); 
			
			relatorioClienteVO.setNome((String) row[0]);
			relatorioClienteVO.setEntrada((BigDecimal) row[1]);
			relatorioClienteVO.setSaida((BigDecimal) row[2]);
			
			BigDecimal soma = relatorioClienteVO.getEntrada().subtract(relatorioClienteVO.getSaida());
			
			relatorioClienteVO.setSoma(soma);
		}
		
		return relatorioClienteVO;
	}
	
	public List<RelatorioSocioVO> getRelatorioSocio(DateBetween date) {
		RelatorioSocioVO relatorioSocioVO;
		List <RelatorioSocioVO> relatorioSocio = new ArrayList<RelatorioSocioVO>();
		
		Iterator<?> rs = this.em.createNamedQuery("MSocio.relatorio")
				.setParameter("dataInicial", date.getDateFrom())
				.setParameter("dataFinal", date.getDateTo()).getResultList().iterator();
		
		while ( rs.hasNext() ) {
			
			relatorioSocioVO = new RelatorioSocioVO();
			Object[] row = (Object[]) rs.next(); 
			
			relatorioSocioVO.setSocio((String) row[0]);
			relatorioSocioVO.setPorcentagem((BigDecimal) row[1]);
			relatorioSocioVO.setVale((BigDecimal) row[2]);
			
			relatorioSocio.add(relatorioSocioVO);
		}
		
		return relatorioSocio;
	}

	
	public RelatorioVO getRelatorioCaixa(DateBetween date) {
		RelatorioVO relatorioVO = new RelatorioVO();
		
		List<String> tipos = new ArrayList<String>();    
//		tipos.add("Socio");
		tipos.add("Escritorio");
		
		Iterator<?> results = this.em.createNamedQuery("Movimento.buscarPorData")
				.setParameter("dataInicial", date.getDateFrom())
				.setParameter("dataFinal", date.getDateTo())
				.setParameter("tipoMovimento", tipos).getResultList().iterator();
		
		while ( results.hasNext() ) {
			Object[] row = (Object[]) results.next(); 
			
			relatorioVO.setEntrada((BigDecimal)row[0]);
			relatorioVO.setSaida((BigDecimal)row[1]);
			
			BigDecimal soma = relatorioVO.getEntrada().subtract(relatorioVO.getSaida());
			relatorioVO.setSoma(soma);
		}
		
		return relatorioVO;
	}

}
