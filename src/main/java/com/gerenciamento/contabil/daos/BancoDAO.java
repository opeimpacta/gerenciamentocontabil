package com.gerenciamento.contabil.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.Banco;

@Repository
public class BancoDAO {
	
	@PersistenceContext
	private EntityManager manager;

	public void save(Banco banco) {
		this.manager.persist(banco);
	}

	public void update(Banco banco) {
		this.manager.merge(banco);
	}

	public void delete(Banco banco) {
		this.manager.remove(banco);
	}

	public List<Banco> list() {

		List<Banco> bancos = this.manager.createQuery("from Banco", Banco.class).getResultList();
		return bancos;
	}

	public Banco findById(Long id) {

		return this.manager.find(Banco.class, id);
	}

}
