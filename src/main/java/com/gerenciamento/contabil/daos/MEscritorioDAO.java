package com.gerenciamento.contabil.daos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gerenciamento.contabil.models.MEscritorio;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.vo.HistoricoAjaxVO;

@Repository
public class MEscritorioDAO {

	@PersistenceContext
	private EntityManager manager;

	public void save(MEscritorio mEscritorio) {
		this.manager.persist(mEscritorio);
	}

	public void update(MEscritorio mEscritorio) {
		mEscritorio = this.manager.merge(mEscritorio);
	}

	public void delete(MEscritorio mescritorio) {
		this.manager.remove(mescritorio);
	}

	public MEscritorio findById(Long id) {
		MEscritorio mEscritorio = this.manager.find(MEscritorio.class, id);
		return mEscritorio;
	}

	public List<MEscritorio> list() {
		List<MEscritorio> mEscritorios = this.manager.createQuery(
				"from MEscritorio", MEscritorio.class).getResultList();
		return mEscritorios;
	}

	public List<MEscritorio> findByDate(DateBetween datas) {
		List<MEscritorio> mEscritorioLista = this.manager
				.createNamedQuery("MEscritorio.buscarPorData",
						MEscritorio.class)
				.setParameter("dataInicial", datas.getDateFrom())
				.setParameter("dataFinal", datas.getDateTo()).getResultList();
		return mEscritorioLista;
	}

	public List<HistoricoAjaxVO> findHistoricoAjax() {
		List<HistoricoAjaxVO> historicos = new ArrayList<>();

		Iterator<?> resultado = this.manager
				.createNamedQuery("MEscritorio.buscarHistoricoDistinto").getResultList()
				.iterator();

		while (resultado.hasNext()) {
			HistoricoAjaxVO historicoAjaxVO = new HistoricoAjaxVO();
			Object coluna = (Object) resultado.next();

			historicoAjaxVO.setHistorico((String) coluna);

			historicos.add(historicoAjaxVO);

		}

		return historicos;
	}
}
