package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.MSocioDAO;
import com.gerenciamento.contabil.models.MSocio;
import com.gerenciamento.contabil.util.DateBetween;

@Controller
@Transactional
@RequestMapping("/msocio")
public class MSocioController {
	@Autowired
	MSocioDAO mSocioDao;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView("movimentos/mSocio");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/list")
	public ModelAndView list(DateBetween dateBetween){
		ModelAndView modelAndView = new ModelAndView("movimentos/mSocio");
		modelAndView.addObject("mSocio", mSocioDao.findByDate(dateBetween));
		return modelAndView;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(MSocio mSocio){
		mSocioDao.update(mSocio);
		return "redirect:list";
	}
	
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save(MSocio mSocio){
		mSocioDao.save(mSocio);
		ModelAndView modelAndView= new ModelAndView("home");
		modelAndView.addObject("sucesso", "sucesso");
		return modelAndView;
	}
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET)
	public ModelAndView findById(@PathVariable Long id){
		MSocio mSocio = mSocioDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("mSocio", mSocio);
		return modelAndView;
	}
	
	@RequestMapping(value="/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		mSocioDao.delete(mSocioDao.findById(id));
		return home();
	}
	
	@RequestMapping(value = "/find-historico", method = RequestMethod.GET)
	public ModelAndView findHistorico() {
		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("hist",
					this.mSocioDao.findHistoricoAjax());
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}
}
