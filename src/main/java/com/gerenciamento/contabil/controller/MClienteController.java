package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.MClienteDAO;
import com.gerenciamento.contabil.models.MCliente;
import com.gerenciamento.contabil.util.DateBetween;

@Controller
@Transactional
@RequestMapping("/mcliente")
public class MClienteController {

	@Autowired
	MClienteDAO mClienteDao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("movimentos/mCliente");
		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView list(DateBetween dateBetween) {
		ModelAndView modelAndView = new ModelAndView("movimentos/mCliente");
		modelAndView.addObject("mCliente", mClienteDao.findByDate(dateBetween));
		return modelAndView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(MCliente mCliente) {
		mClienteDao.update(mCliente);
		return "redirect:list";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(MCliente mCliente) {
		mClienteDao.save(mCliente);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("sucesso", "sucesso");
		return modelAndView;
	}

	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public ModelAndView findById(@PathVariable Long id) {
		MCliente mCliente = mClienteDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("mCliente", mCliente);
		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}")
	public ModelAndView delete(@PathVariable Long id) {
		mClienteDao.delete(mClienteDao.findById(id));
		return home();
	}

	@RequestMapping(value = "/find-historico", method = RequestMethod.GET)
	public ModelAndView findHistorico() {
		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("hist",
					this.mClienteDao.findHistoricoAjax());
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}
}
