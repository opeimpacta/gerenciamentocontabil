package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.SocioDAO;
import com.gerenciamento.contabil.models.Socio;

@Controller
@Transactional
@RequestMapping("/socio")
public class SociosController {

	@Autowired
	SocioDAO socioDAO;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create() {
		return "socios/cadastroSocios";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(@ModelAttribute("socio") Socio socio) {
		socioDAO.save(socio);
		return "redirect:socio/list";

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("socios/listSocios");
		modelAndView.addObject("socios", socioDAO.list());
		return modelAndView;
	}

	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public ModelAndView findUser(@PathVariable Long id) {
		Socio socio = socioDAO.findById(id);
		ModelAndView modelAndView = new ModelAndView("socios/editaSocio");
		modelAndView.addObject("socio", socio);
		return modelAndView;

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Socio socio) {
		socioDAO.update(socio);
		return "redirect:list";
	}

	@RequestMapping(value = "/delete/{id}")
	public ModelAndView delete(@PathVariable Long id) {
		socioDAO.delete(socioDAO.findById(id));
		return list();
	}

	@RequestMapping(value = "/find-cpf/{cpf}", method = RequestMethod.GET)
	public ModelAndView findUserByCpf(@PathVariable String cpf) {

		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("user-cpf", this.socioDAO.findByCPF(cpf));
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}

	@RequestMapping(value = "/find-email/{email}", method = RequestMethod.GET)
	public ModelAndView findByEmail(@PathVariable String email) {

		ModelAndView modelAndView = new ModelAndView("403");

			modelAndView.addObject("socio-email",
					this.socioDAO.findByEmail(email));


		return modelAndView;

	}
}
