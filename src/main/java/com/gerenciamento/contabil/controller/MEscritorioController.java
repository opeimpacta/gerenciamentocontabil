package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.MEscritorioDAO;
import com.gerenciamento.contabil.models.MEscritorio;
import com.gerenciamento.contabil.util.DateBetween;

@Controller
@Transactional
@RequestMapping("/mescritorio")
public class MEscritorioController {
	
	@Autowired
	MEscritorioDAO mEscritorioDao;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView homeMEscritorio(){
		ModelAndView modelAndView = new ModelAndView("movimentos/mEscritorio");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/list")
	public ModelAndView list(DateBetween dateBetween){
		ModelAndView modelAndView = new ModelAndView("movimentos/mEscritorio");
		modelAndView.addObject("mEscritorioDao", mEscritorioDao.findByDate(dateBetween));
		return modelAndView;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(MEscritorio mEscritorio){
		mEscritorioDao.update(mEscritorio);
		return "redirect:list";
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(MEscritorio mEscritorio){
		mEscritorioDao.save(mEscritorio);
		ModelAndView modelAndView= new ModelAndView("home");
		modelAndView.addObject("sucesso", "sucesso");
		return modelAndView;
	}
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET)
	public ModelAndView findById(@PathVariable Long id){
		MEscritorio mEscritorio = mEscritorioDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("movimentos/editarMEscritorio");
		modelAndView.addObject("mEscritorio", mEscritorio);
		return modelAndView;
	}
	
	@RequestMapping(value="/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		mEscritorioDao.delete(mEscritorioDao.findById(id));
		return homeMEscritorio();
	}
	
	@RequestMapping(value = "/find-historico", method = RequestMethod.GET)
	public ModelAndView findHistorico() {
		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("hist",
					this.mEscritorioDao.findHistoricoAjax());
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}

}
