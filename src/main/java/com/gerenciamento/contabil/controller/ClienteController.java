package com.gerenciamento.contabil.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.ClienteDAO;
import com.gerenciamento.contabil.models.Cliente;


@Controller
@Transactional
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteDAO clienteDao;
	
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(@ModelAttribute Cliente cliente){
		return "clientes/cadastroCliente";
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(Cliente cliente){
		clienteDao.update(cliente);
		return "redirect:list";
	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	public String save(Cliente cliente){
		clienteDao.save(cliente);
		return "redirect:cliente/list";
	}
	
	@RequestMapping(value = "/list", method= RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("clientes/listClientes");
		modelAndView.addObject("clientes", clienteDao.list());
		return modelAndView;
	}
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET)
	public ModelAndView findById(@PathVariable Long id){
		Cliente cliente = clienteDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("clientes/editarCliente");
		modelAndView.addObject("cliente", cliente);
		return modelAndView;
		
	}
	
	@RequestMapping(value="/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		clienteDao.delete(clienteDao.findById(id));
		return list();
	}


}
