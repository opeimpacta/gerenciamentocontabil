package com.gerenciamento.contabil.controller;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.BancoDAO;
import com.gerenciamento.contabil.models.Banco;

@Controller
@Transactional
@RequestMapping("/banco")
public class BancoController {
	
	@Autowired
	BancoDAO bancoDao;
	
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView create(@ModelAttribute Banco banco){
		ModelAndView modelAndView = new ModelAndView("bancos/cadastroBancos");
		return modelAndView;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(Banco banco){
		bancoDao.update(banco);
		return "redirect:list";
	}
	
	@RequestMapping(value="/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		bancoDao.delete(bancoDao.findById(id));
		return list();
	}
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET)
	public ModelAndView findUser(@PathVariable Long id){
		Banco banco = bancoDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("bancos/editaBancos");
		modelAndView.addObject("banco", banco);
		return modelAndView;
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView save(@ModelAttribute @Valid Banco banco, BindingResult bindingResult){
		if (bindingResult.hasErrors()) {
			return create(banco);
		}
		
		bancoDao.save(banco);
		ModelAndView modelAndView = new ModelAndView("redirect:banco/list");
		return modelAndView;
	}
	
	@RequestMapping(value = "/list", method= RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("bancos/listBancos");
		modelAndView.addObject("bancos", bancoDao.list());
		return modelAndView;
	}
}
