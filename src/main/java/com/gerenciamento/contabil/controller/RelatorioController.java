package com.gerenciamento.contabil.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.service.RelatorioManager;
import com.gerenciamento.contabil.util.DateBetween;
import com.gerenciamento.contabil.util.Util;
import com.gerenciamento.contabil.vo.RelatorioBancoVO;
import com.gerenciamento.contabil.vo.RelatorioClienteVO;
import com.gerenciamento.contabil.vo.RelatorioSocioVO;
import com.gerenciamento.contabil.vo.RelatorioVO;

@Controller
@RequestMapping("relatorio")
public class RelatorioController {

	@Autowired
	RelatorioManager relatorio;
	
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public ModelAndView list(DateBetween date){
		
		List<RelatorioSocioVO> relatorioSocio = relatorio.getRelatorioSocio(date);
		RelatorioClienteVO relatorioCliente = relatorio.getRelatorioCliente(date);
		RelatorioVO relatorioCaixa = relatorio.getRelatorioCaixa(date);
		List<RelatorioBancoVO> relatorioBanco = relatorio.getRelatorioBanco(date);
		
		ModelAndView modelAndView = new ModelAndView("home");
		
		modelAndView.addObject("relatorio_socio", relatorioSocio);
		modelAndView.addObject("relatorio_cliente", relatorioCliente);
		
		modelAndView.addObject("relatorio_caixa", relatorioCaixa);
		modelAndView.addObject("soma_total_socio", Util.convertToMoney(relatorio.getSomaTotalSocio(relatorioCliente, relatorioSocio)));
		modelAndView.addObject("relatorio_banco", relatorioBanco);
		modelAndView.addObject("soma_total_banco", Util.convertToMoney(relatorio.getSomaTotalBanco(relatorioBanco)));
		
		return modelAndView;
	}
	
	@RequestMapping(value="/geral", method=RequestMethod.GET)
	public ModelAndView relatorioCaixa(){
		ModelAndView modelAndView = new ModelAndView("relatorios/retolatorioGeral");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/caixa", method=RequestMethod.GET)
	public ModelAndView relatorioGeral(){
		ModelAndView modelAndView = new ModelAndView("relatorios/relatorioEscritorio");
		return modelAndView;
	}
	
	
	
}
