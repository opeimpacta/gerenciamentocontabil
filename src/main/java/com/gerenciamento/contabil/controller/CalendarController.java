package com.gerenciamento.contabil.controller;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.EventsDAO;
import com.gerenciamento.contabil.models.Events;
import com.gerenciamento.contabil.models.User;
import com.gerenciamento.contabil.util.DateStartEnd;

@Controller
@Transactional
@RequestMapping("/calendar")
public class CalendarController {
	
	private static final Logger logger = LoggerFactory.getLogger(CalendarController.class);
	
	@Autowired
	EventsDAO eventsDao;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView home(Events events){
		ModelAndView modelAndView = new ModelAndView("calendar/calendar");
		return modelAndView;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(Events events){
		events.setUser(new User());
		
		eventsDao.update(events);
		return "home";
	}
	
	@RequestMapping(value="/delete/{id}")
	public ModelAndView delete(@PathVariable Long id){
		eventsDao.delete(eventsDao.findById(id));
		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET)
	public ModelAndView findUser(@PathVariable Long id){
		Events events = eventsDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("event", events);
		return modelAndView;
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView save(Events events){
		
		events.setUser(new User());
		
		eventsDao.save(events);
		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}
	
	@RequestMapping(value = "/list", method= RequestMethod.GET)
	public ModelAndView list(DateStartEnd date){
		
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("events", eventsDao.findByDate(date, User.getUserLogged()));
		return modelAndView;
	}

}
