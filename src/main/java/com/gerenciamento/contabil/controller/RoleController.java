package com.gerenciamento.contabil.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.RoleDAO;


@Controller
@Transactional
@RequestMapping("/role")
public class RoleController {
	
	@Autowired
	RoleDAO role;
	
	@RequestMapping(value = "/list", method= RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("roles", role.list());
		return modelAndView;
	}
	
}
