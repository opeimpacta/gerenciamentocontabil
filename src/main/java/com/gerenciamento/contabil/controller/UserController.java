package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.SocioDAO;
import com.gerenciamento.contabil.daos.UserDAO;
import com.gerenciamento.contabil.models.Socio;
import com.gerenciamento.contabil.models.User;
import com.gerenciamento.contabil.util.CustomGenericException;

@Controller
@Transactional
@RequestMapping(value = "/user")
public class UserController {

	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@Autowired
	MessageSource message;

	@Autowired
	UserDAO userDao;

	@Autowired
	SocioDAO socioDao;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String homeCreate() {
		return "usuarios/cadastroUsuarios";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String homeUpdate(User user) {
		userDao.update(user);
		return "redirect:list";
	}

	@RequestMapping(value = "/delete/{login}", method = RequestMethod.GET)
	public ModelAndView homeDelete(@PathVariable String login) {
		if (!"".equals(login)) {
			Socio socio = null;
			try {
				socio = socioDao.findByUser(userDao.findUser(login));
			} catch (NoResultException e) {

			}

			if (!(socio == null)) {
				socioDao.delete(socio);

			} else {
				userDao.deletar(userDao.findUser(login));
			}
			login = "";
		}
		return list();
	}

	@RequestMapping(value = "/find/{login}", method = RequestMethod.GET)
	public ModelAndView findUser(@PathVariable String login) {
		User user = userDao.findUser(login);
		ModelAndView modelAndView = new ModelAndView("usuarios/editaUsuarios");
		modelAndView.addObject("user", user);
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(User user) throws Exception {

		try {
			userDao.saveUser(user);
		} catch (PersistenceException e) {
			throw new CustomGenericException("errorLogin", e.getMessage(),
					"user/create", message.getMessage("message.user.login",
							null, null));
		}

		return "redirect:user/list";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("usuarios/listUsuario");
		modelAndView.addObject("users", userDao.list());
		return modelAndView;
	}

	@ExceptionHandler(CustomGenericException.class)
	public ModelAndView handleCustomException(CustomGenericException ex) {

		ModelAndView model = new ModelAndView("redirect:"
				+ ex.getPageRedirect());
		model.addObject("exceptionUser", ex);

		return model;
	}

	// Total control - setup a model and return the view name yourself. Or
	// consider
	// subclassing ExceptionHandlerExceptionResolver (see below).
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception exception) {
		logger.error("Request: " + req.getRequestURL() + " raised " + exception);

		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", exception);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName("error");
		return mav;
	}

	@RequestMapping(value = "/find-email/{email}", method = RequestMethod.GET)
	public ModelAndView findUserByEmail(@PathVariable String email) {

		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("user-email",
					this.userDao.findByEmail(email));
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}

}
