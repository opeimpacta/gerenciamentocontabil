package com.gerenciamento.contabil.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.service.RelatorioDashManager;

@Controller
@RequestMapping("relatoriodash")
public class RelatorioDashController {
	@Autowired
	RelatorioDashManager relatorioDash;

	@RequestMapping(value = "/list-escritorio", method = RequestMethod.GET)
	public ModelAndView listEscritorio() {
		ModelAndView modelAndView = new ModelAndView("403");
		modelAndView.addObject("list-escritorio",
				this.relatorioDash.getDashEscritorio());
		return modelAndView;
	}

	@RequestMapping(value = "/list-socio", method = RequestMethod.GET)
	public ModelAndView listSocio() {
		ModelAndView modelAndView = new ModelAndView("403");
		modelAndView.addObject("list-socio",
				this.relatorioDash.getDashSocio());
		return modelAndView;
	}

	@RequestMapping(value = "/list-cliente", method = RequestMethod.GET)
	public ModelAndView listCliente() {
		ModelAndView modelAndView = new ModelAndView("403");
		modelAndView.addObject("list-cliente",
				this.relatorioDash.getDashCliente());
		return modelAndView;
	}
}
