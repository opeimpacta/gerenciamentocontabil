package com.gerenciamento.contabil.controller;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gerenciamento.contabil.daos.MBancoDAO;
import com.gerenciamento.contabil.models.MBanco;
import com.gerenciamento.contabil.util.DateBetween;

@Controller
@Transactional
@RequestMapping("mbanco")
public class MBancoController {

	@Autowired
	MBancoDAO mBancoDao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("movimentos/mBanco");
		return modelAndView;
	}

	@RequestMapping(value = "/list")
	public ModelAndView list(DateBetween dateBetween) {
		ModelAndView modelAndView = new ModelAndView("movimentos/mBanco");
		modelAndView.addObject("mBanco", mBancoDao.findByDate(dateBetween));
		return modelAndView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(MBanco mBanco) {
		mBancoDao.update(mBanco);
		return "redirect:list";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(MBanco mBanco) {
		mBancoDao.save(mBanco);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("sucesso", "sucesso");
		return modelAndView;
	}

	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public ModelAndView findById(@PathVariable Long id) {
		MBanco mBanco = mBancoDao.findById(id);
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("mBanco", mBanco);
		return modelAndView;
	}

	@RequestMapping(value = "/delete/{id}")
	public ModelAndView delete(@PathVariable Long id) {
		mBancoDao.delete(mBancoDao.findById(id));
		return home();
	}

	@RequestMapping(value = "/find-historico", method = RequestMethod.GET)
	public ModelAndView findHistorico() {
		ModelAndView modelAndView = new ModelAndView("403");

		try {
			modelAndView.addObject("hist", this.mBancoDao.findHistoricoAjax());
		} catch (NoResultException e) {

			// Não retorna exceção
		}

		return modelAndView;

	}
}
