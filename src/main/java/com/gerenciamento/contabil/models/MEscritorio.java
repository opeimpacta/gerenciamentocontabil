package com.gerenciamento.contabil.models;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("Escritorio")
@NamedQueries({
		@NamedQuery(name = "MEscritorio.buscarPorData", query = "SELECT d FROM MEscritorio d WHERE d.data BETWEEN :dataInicial AND :dataFinal"),
		@NamedQuery(name = "MEscritorio.buscarConsolidadoPorMes", query = "Select CONCAT(MONTH(m.data),'/',year(m.data)), "
				+ "SUM(m.entrada), SUM(m.saida) "
				+ "from MEscritorio m where m.data >= :dataInicial group by CONCAT(MONTH(m.data),'/',year(m.data)) order by "
				+ "month(m.data), year(m.data) asc"),
		@NamedQuery(name = "MEscritorio.buscarHistoricoDistinto", query = "SELECT DISTINCT m.historico "
				+ "FROM MEscritorio m") })
public class MEscritorio extends Movimento {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5215232110918090765L;
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_cliente", nullable = true)
	private Cliente cliente;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
