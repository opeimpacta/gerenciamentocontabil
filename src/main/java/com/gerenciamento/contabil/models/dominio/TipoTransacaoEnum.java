package com.gerenciamento.contabil.models.dominio;

/**
 * 
 * @author Rafael
 *
 */
public enum TipoTransacaoEnum {
	RETIRADA("Retirada"),
	INVESTIMENTO("Investimento"),
	PAGAMENTO("Pagamento");
	
//	CADASTRO_DE_CLIENTE("Cadastro de cliente"), Esses farão parte do histórico sistemico
//	CADASTRO_DE_SOCIO("Cadastro de sócio"),
//	CADASTRO_DE_USUARIO("Cadastro de usuário"),
//	INATIVAR_USUARIO("Inativar usuário");
//	
	private String tipo;
	
	TipoTransacaoEnum(String tipo){
	this.tipo=tipo;	
	}

	public String getTipo() {
		return tipo;
	}
	
	@Override
	public String toString(){
		return this.tipo;
	}
}
