package com.gerenciamento.contabil.models;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Rafael
 *
 */
@NamedQueries({ @NamedQuery(name = "Cliente.buscaPorCpf", query = "select c from Cliente c where c.cpf = :cpf") })
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 150)
	private String nome;

	@Column(length = 40)
	private String email;

	@Column(length = 14)
	private String cnpj;

	@Column(name = "incricao_estadual")
	private String iE; // inscrição estadual

	@Column(name = "inscricao_municipal")
	private String iM; // inscrição Municipal

	@Column(length = 50)
	private String jucesp; // sei lá que merda é essa

	@Column(name = "numero_regsitro", length = 30)
	private String numeroRegistro;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_registro", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Calendar dataRegistro;

	@Column(name = "socio_responsavel", length = 60)
	private String socioResponsavel;

	@Column(length = 11, nullable = true)
	private String cpf;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_telefone", referencedColumnName = "id")
	private Telefone telefones;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_endereco", referencedColumnName = "id")
	private Endereco endereco;

	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_socio")
	private Socio socio;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCnpj() {
		return cnpj;
	}

	/**
	 * 
	 * @param cnpj
	 *            Este método recebe o CNPJ tratando o mesmo antes da
	 *            persistencia não existe necessidade de tirar os ".","/" e "-".
	 */
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj.replace(".", "").replace("-", "").replace("/", "");
	}

	public String getiE() {
		return iE;
	}

	public void setiE(String iE) {
		this.iE = iE;
	}

	public String getiM() {
		return iM;
	}

	public void setiM(String iM) {
		this.iM = iM;
	}

	public String getJucesp() {
		return jucesp;
	}

	public void setJucesp(String jucesp) {
		this.jucesp = jucesp;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	// public void setDataRegistro(String dataRegistro) {
	//
	//
	// SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	// Date dt;
	//
	// try {
	// dt = df.parse(dataRegistro);
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// dt = new Date();
	// }
	// this.dataRegistro = dt;
	// }

	public String getSocioResponsavel() {
		return socioResponsavel;
	}

	public void setSocioResponsavel(String socioResponsavel) {
		this.socioResponsavel = socioResponsavel;
	}

	public String getCpf() {
		return cpf;
	}

	/**
	 * 
	 * @param cpf
	 *            Este método recebe o CPF tratando o mesmo antes da
	 *            persistencia não existe necessidade de tirar os "." e "-".
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf.replace(".", "").replace("-", "");
	}

	public Telefone getTelefones() {
		return telefones;
	}

	public void setTelefones(Telefone telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Calendar getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Calendar dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

}
