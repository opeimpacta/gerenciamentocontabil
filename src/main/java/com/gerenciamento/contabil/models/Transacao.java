package com.gerenciamento.contabil.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.gerenciamento.contabil.models.dominio.TipoTransacaoEnum;

/**
 * 
 * @author Rafael
 * 
 *         Esta entidade é responsável por registrar todos os históricos feitos
 *         no sistema com inclusão de usuario
 */

@Entity
@Table(name = "transacoes")
public class Transacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "id_usuario")
	private User usuario;

	@Column(nullable = false, name = "tipo_transacao")
	@Enumerated(EnumType.STRING)
	private TipoTransacaoEnum tipoTransacao;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_da_transacao", nullable = false)
	private Calendar dataDaTransacao;

	@Column(name = "valor_da_transação", nullable = false)
	private BigDecimal valorDaTransacao;

	@OneToOne
	@JoinColumn(name="id_banco")
	private Banco banco;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUsuario() {
		return usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

	public TipoTransacaoEnum getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacaoEnum tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public BigDecimal getValorDaTransacao() {
		return valorDaTransacao;
	}

	public void setValorDaTransacao(BigDecimal valorDaTransacao) {
		this.valorDaTransacao = valorDaTransacao;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transacao other = (Transacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Calendar getDataDaTransacao() {
		return dataDaTransacao;
	}

	public void setDataDaTransacao(Calendar dataDaTransacao) {
		this.dataDaTransacao = dataDaTransacao;
	}

}
