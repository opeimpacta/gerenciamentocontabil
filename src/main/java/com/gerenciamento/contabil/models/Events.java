package com.gerenciamento.contabil.models;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gerenciamento.contabil.converters.BooleanToYNStringConverter;
import com.gerenciamento.contabil.converters.JsonCalendarTimeZoneSerealizer;

@Entity
@Table(name = "events")
@NamedQueries({ @NamedQuery(name = "Events.buscarPorData", query = "SELECT e FROM Events e WHERE e.start BETWEEN :dataInicial AND :dataFinal "
																								+ "AND e.user.login = :idUser OR e.isGeneralEvent = :eventoGeral") })
public class Events implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(length = 255)
	private String title;
	
	@Lob
	private String description;
	
	@Column(length = 7)
	private String color;
	

	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
	@Temporal(TemporalType.TIMESTAMP)	
	private Calendar start;
	
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
	@Temporal(TemporalType.TIMESTAMP)	
	private Calendar end;
	
	@Column(length = 10)
	private String dateUpdate;
	
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_user", nullable=false)
	private User user;
	
	
	@Column(name="evento_geral")
	private char isGeneralEvent = 'N';
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	@JsonSerialize(using = JsonCalendarTimeZoneSerealizer.class)
	public Calendar getStart() {
		return start;
	}

	public void setStart(Calendar start) {
		this.start = start;
	}
	
	@JsonSerialize(using = JsonCalendarTimeZoneSerealizer.class)
	public Calendar getEnd() {
		return end;
	}

	public void setEnd(Calendar end) {
		this.end = end;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public char getIsGeneralEvent() {
		return isGeneralEvent;
	}

	public void setIsGeneralEvent(char isGeneralEvent) {
		this.isGeneralEvent = isGeneralEvent;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = User.getUserLogged();
	}
	
}

