package com.gerenciamento.contabil.models;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue("Socio")
@NamedQueries({
		@NamedQuery(name = "MSocio.buscarPorData", query = "SELECT d FROM MSocio d WHERE d.data BETWEEN :dataInicial AND :dataFinal"),
		@NamedQuery(name = "MSocio.relatorio", query = "SELECT s.user.name, "
				+ "s.porcentagem, " + "sum(m.saida) + 0 " + "FROM Socio s "
				+ "LEFT JOIN s.mSocio m "
				+ "WHERE m.data BETWEEN :dataInicial AND :dataFinal "
				+ "GROUP BY s.user.name"),
		@NamedQuery(name = "MSocio.buscarConsolidadoPorMes", query = "Select CONCAT(MONTH(m.data),'/',year(m.data)), "
				+ "SUM(m.entrada), SUM(m.saida) "
				+ "from MSocio m where m.data >= :dataInicial group by CONCAT(MONTH(m.data),'/',year(m.data)) order by "
				+ "month(m.data), year(m.data) asc"),
		@NamedQuery(name = "MSocio.buscarHistoricoDistinto", query = "SELECT DISTINCT m.historico "
				+ "FROM MSocio m")

})
public class MSocio extends Movimento {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_socio", nullable = true)
	private Socio socio;

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

}
