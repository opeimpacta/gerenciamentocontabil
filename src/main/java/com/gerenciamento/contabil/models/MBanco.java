package com.gerenciamento.contabil.models;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("Banco")
@NamedQueries({
		@NamedQuery(name = "MBanco.buscarPorData", query = "SELECT b FROM MBanco b WHERE b.data BETWEEN :dataInicial AND :dataFinal"),
		@NamedQuery(name = "MBanco.relatorio", query = "SELECT b.nomeDoBanco, sum(m.entrada), sum(m.saida) "
				+ "FROM MBanco m "
				+ "JOIN m.banco b "
				+ "WHERE m.data BETWEEN :dataInicial AND :dataFinal "
				+ "GROUP BY b.nomeDoBanco"),
		@NamedQuery(name = "MBanco.buscarHistoricoDistinto", query = "SELECT DISTINCT m.historico "
				+ "FROM MBanco m") })
public class MBanco extends Movimento {

	private static final long serialVersionUID = 1L;

	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_banco", nullable = true)
	private Banco banco;

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

}
