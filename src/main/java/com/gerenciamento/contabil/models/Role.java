package com.gerenciamento.contabil.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;

@Entity
public class Role implements GrantedAuthority{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String name;
	
	private String description;

	@Override
	public String getAuthority() {
	
		return name;
	}
	
	public void setRole(String papel){
		this.name = papel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
