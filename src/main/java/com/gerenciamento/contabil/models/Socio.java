package com.gerenciamento.contabil.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "socio")
@NamedQueries({
		@NamedQuery(name = "Socio.buscarPorUsuario", query = "SELECT s FROM Socio s WHERE s.user = :user"),
		@NamedQuery(name = "Socio.buscarPorCPF", query = "SELECT c FROM Socio c WHERE c.cpf = :cpf") })
public class Socio implements Serializable {

	private static final long serialVersionUID = -8341110224903583257L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 11)
	private String cpf;

	private BigDecimal porcentagem;

	// @PrimaryKeyJoinColumn(name="id_telefone", referencedColumnName="id")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_telefone", referencedColumnName = "id")
	private Telefone telefones;

	/*
	 * Cascade - Define açoes automatizadas no relacionamento, nesse caso ao
	 * apagar um socio vai apagar também um user optional - Com o valor igual a
	 * false ao cadastrar um socio é obrigatorio a cadastrar um user, sem essa
	 * opção poderia existir um socio sem usuario fetch - O valor padrão é
	 * EAGER, ou seja ao carregar um socio já será feita a consulta relacionada
	 * ao user de modo automatico orphanRemoval - define que uma entidade
	 * depende, caso não tenha relacionamento, será removido do banco de dados.
	 * Caso exista um user sem socio, esse user será removido
	 * 
	 * @PrimaryKeyJoinColumn – essa anotação indica ao JPA que, para encontrar
	 * um objeto User basta procurar um registro com o mesmo ID do Customer. Ou
	 * seja, indica que um User vai ter o mesmo ID que seu Customer.
	 */

	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_user", referencedColumnName = "login")
	private User user;

	@JsonIgnore
	@OneToMany(mappedBy = "socio")
	private List<MSocio> mSocio;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Socio other = (Socio) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf.replace(".", "").replace("-", "");
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Telefone getTelefones() {
		return telefones;
	}

	public void setTelefones(Telefone telefones) {
		this.telefones = telefones;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}

	public List<MSocio> getmSocio() {
		return mSocio;
	}

	public void setmSocio(List<MSocio> mSocio) {
		this.mSocio = mSocio;
	}

}