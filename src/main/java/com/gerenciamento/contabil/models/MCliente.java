package com.gerenciamento.contabil.models;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("Cliente")
@NamedQueries({
		@NamedQuery(name = "MCliente.buscarPorData", query = "SELECT d FROM MCliente d WHERE d.data BETWEEN :dataInicial AND :dataFinal"),
		@NamedQuery(name = "MCliente.relatorio", query = "SELECT c.tipo_movimento, sum(c.entrada), sum(c.saida) "
				+ "FROM MCliente c  "
				+ "WHERE c.data BETWEEN :dataInicial AND :dataFinal "
				+ "GROUP BY c.tipo_movimento"),
		@NamedQuery(name = "MCliente.buscarConsolidadoPorMes", query = "Select CONCAT(MONTH(m.data),'/',year(m.data)), "
				+ "SUM(m.entrada), SUM(m.saida) "
				+ "from MCliente m where m.data >= :dataInicial group by CONCAT(MONTH(m.data),'/',year(m.data)) order by "
				+ "month(m.data), year(m.data) asc"),
		@NamedQuery(name = "MCliente.buscarHistoricoDistinto", query = "SELECT DISTINCT m.historico "
				+ "FROM MCliente m") })
public class MCliente extends Movimento {

	/**
	 * 
	 */
	private static final long serialVersionUID = 449188810759159549L;
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_cliente", nullable = true)
	private Cliente cliente;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
