package com.gerenciamento.contabil.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gerenciamento.contabil.converters.JsonCalendarSerializer;
import com.gerenciamento.contabil.converters.JsonMoneyDeserealizer;
import com.gerenciamento.contabil.converters.JsonMoneySerializer;

/**
 * @author Rafael
 */
@Entity
@Table(name = "movimento")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_movimento")
@NamedQueries({ @NamedQuery(name = "Movimento.buscarPorData", query = "SELECT sum(m.entrada), sum(m.saida) "
		+ "FROM Movimento m "
		+ "WHERE m.data BETWEEN :dataInicial AND :dataFinal "
		+ "AND m.tipo_movimento in :tipoMovimento") })
public class Movimento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Calendar data;

	@Column(length = 60, name = "historico")
	private String historico;

	@JsonDeserialize(using = JsonMoneyDeserealizer.class)
	@Column(columnDefinition = "decimal(19,2) default 0")
	private BigDecimal entrada = new BigDecimal("0");

	@Column(columnDefinition = "decimal(19,2) default 0")
	@JsonDeserialize(using = JsonMoneyDeserealizer.class)
	private BigDecimal saida = new BigDecimal("0");

	@Column(insertable = false, updatable = false)
	private String tipo_movimento;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimento other = (Movimento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getEntrada() {
		return entrada;
	}

	public void setEntrada(BigDecimal entrada) {
		this.entrada = entrada;
	}

	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getSaida() {
		return saida;
	}

	public void setSaida(BigDecimal saida) {
		this.saida = saida;
	}

	@JsonSerialize(using = JsonCalendarSerializer.class)
	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getTipo_movimento() {
		return tipo_movimento;
	}

	public void setTipo_movimento(String tipo_movimento) {
		this.tipo_movimento = tipo_movimento;
	}

}
