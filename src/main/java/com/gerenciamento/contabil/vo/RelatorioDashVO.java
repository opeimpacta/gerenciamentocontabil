package com.gerenciamento.contabil.vo;

import java.math.BigDecimal;

public class RelatorioDashVO {
	private String data;
	private BigDecimal entrada;
	private BigDecimal saida;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public BigDecimal getEntrada() {
		return entrada;
	}

	public void setEntrada(BigDecimal entrada) {
		this.entrada = entrada;
	}

	public BigDecimal getSaida() {
		return saida;
	}

	public void setSaida(BigDecimal saida) {
		this.saida = saida;
	}

}
