package com.gerenciamento.contabil.vo;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gerenciamento.contabil.converters.JsonMoneySerializer;

public class RelatorioSocioVO {
	private String socio;
	
	private BigDecimal porcentagem;
	
	private BigDecimal lucro;
	
	private BigDecimal lucro_final;
	
	private BigDecimal vale;

	public String getSocio() {
		return socio;
	}

	public void setSocio(String socio) {
		this.socio = socio;
	}

	public BigDecimal getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(BigDecimal porcentagem) {
		if(porcentagem == null){
			porcentagem = new BigDecimal(0);
		}
		this.porcentagem = porcentagem;
	}
	
	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getLucro() {
		return lucro;
	}

	public void setLucro(BigDecimal lucro) {
		if(lucro == null){
			lucro = new BigDecimal(0);
		}
		this.lucro = lucro;
	}
	
	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getLucro_final() {
		return lucro_final;
	}

	public void setLucro_final(BigDecimal lucro_final) {
		if(lucro_final == null){
			lucro_final = new BigDecimal(0);
		}
		this.lucro_final = lucro_final;
	}
	
	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getVale() {
		return vale;
	}

	public void setVale(BigDecimal vale) {
		if(vale == null){
			vale = new BigDecimal(0);
		}
		this.vale = vale;
	}
}
