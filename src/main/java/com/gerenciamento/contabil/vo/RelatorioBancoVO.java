package com.gerenciamento.contabil.vo;

public class RelatorioBancoVO extends RelatorioVO{
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
