package com.gerenciamento.contabil.vo;

public class RelatorioClienteVO extends RelatorioVO{
	
	private String nome;
	
	private static final String NOME = "Cliente";

	public String getNome() {
		
		if(this.nome == null){
			this.nome = NOME;
		}
		return this.nome;
	}

	public void setNome(String nome) {
		
		if(nome == null){
			nome = NOME;
		}
		
		this.nome = nome;
	}
	


}
