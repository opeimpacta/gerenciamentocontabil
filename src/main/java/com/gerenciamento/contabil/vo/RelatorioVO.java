package com.gerenciamento.contabil.vo;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gerenciamento.contabil.converters.JsonMoneySerializer;

public class RelatorioVO {
	
	private BigDecimal entrada;
	
	private BigDecimal saida;
	
	private BigDecimal soma;
	
	private static final BigDecimal ZERO = new BigDecimal(0);
	
	
	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getEntrada() {
		
		if(this.entrada == null){
			this.entrada = ZERO;
		}
		return this.entrada;
	}


	public void setEntrada(BigDecimal entrada) {
		
		if(entrada == null){
			entrada = ZERO;
		}
		
		this.entrada = entrada;
	}

	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getSaida() {
		if(this.saida == null){
			this.saida = ZERO;
		}
		return this.saida;
	}


	public void setSaida(BigDecimal saida) {
		if(saida == null){
			saida = ZERO;
		}
		
		this.saida = saida;
	}

	@JsonSerialize(using = JsonMoneySerializer.class)
	public BigDecimal getSoma() {
		
		if(this.soma == null){
			this.soma = ZERO;
		}
		return this.soma;
	}


	public void setSoma(BigDecimal soma) {
		if(soma == null){
			soma = ZERO;
		}
		
		this.soma = soma;
	}

}
