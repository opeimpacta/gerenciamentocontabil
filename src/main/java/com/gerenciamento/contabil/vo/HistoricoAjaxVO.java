package com.gerenciamento.contabil.vo;

public class HistoricoAjaxVO {
	private String historico;

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

}
