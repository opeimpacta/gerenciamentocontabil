package com.gerenciamento.contabil.util;

import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

public class DateStartEnd {
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Calendar start;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Calendar end;
	
	public Calendar getStart() {
		return start;
	}
	public void setStart(Calendar start) {
		this.start = start;
	}
	public Calendar getEnd() {
		return end;
	}
	public void setEnd(Calendar end) {
		this.end = end;
	}
}
