package com.gerenciamento.contabil.util;

public class CustomGenericException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String errCode;
	private String errMsg;
	private String pageRedirect;
	private String errMsgTrat;

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public String getPageRedirect() {
		return pageRedirect;
	}

	public void setPageRedirect(String pageRedirect) {
		this.pageRedirect = pageRedirect;
	}


	public CustomGenericException(String errCode, String errMsg) {
		this.errCode = errCode;
		this.errMsg = errMsg;
	}
	
	public CustomGenericException(String errCode, String errMsg, String pageRedirect, String errMsgTrat) {
		this.errCode = errCode;
		this.errMsg = errMsg;
		this.pageRedirect = pageRedirect;
		this.errMsgTrat = errMsgTrat;
	}

	public String getErrMsgTrat() {
		return errMsgTrat;
	}

	public void setErrMsgTrat(String errMsgTrat) {
		this.errMsgTrat = errMsgTrat;
	}

}