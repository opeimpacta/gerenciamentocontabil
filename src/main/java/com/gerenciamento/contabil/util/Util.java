package com.gerenciamento.contabil.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

import com.gerenciamento.contabil.vo.RelatorioSocioVO;
import com.gerenciamento.contabil.vo.RelatorioVO;

public class Util {
	
	public static List<RelatorioSocioVO> calcPorcentagemLucroSocio(List<RelatorioSocioVO> relatorioSocios, RelatorioVO relatorioCaixa ){

		BigDecimal porcentagem, comissao, desconto;
		BigDecimal limite_porcentagem = new BigDecimal(100.0);
		
		
		for (RelatorioSocioVO relatorioSocio : relatorioSocios) {
			
			porcentagem = relatorioSocio.getPorcentagem().divide(limite_porcentagem);
			comissao = porcentagem.multiply(relatorioCaixa.getSoma());
//			comissao = porcentagem.multiply(relatorioCaixa.getTotal_entrada());
			desconto = comissao.subtract(relatorioSocio.getVale());
			
			relatorioSocio.setLucro(comissao);
			relatorioSocio.setLucro_final(desconto);
			
			
		}
		
		return relatorioSocios;
	}
	
	public static String convertToMoney(Object money){
		
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		String moneyValue = moneyFormat.format(money); // deve mostrar "R$ 00,00"
		
		return moneyValue;
	}
}
