package com.gerenciamento.contabil.util;

import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

public class DateBetween {
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Calendar dateFrom;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Calendar dateTo;
	
	
	public Calendar getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Calendar dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Calendar getDateTo() {
		return dateTo;
	}
	public void setDateTo(Calendar dateTo) {
		this.dateTo = dateTo;
	}
}
